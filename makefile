include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C odl all
	$(MAKE) -C mod-bridging-uci/src all
	$(MAKE) -C mod-bridging-ioctl/src all
	$(MAKE) -C mod-bridging-dummy/src all
	$(MAKE) -C mod-vlan-dummy/src all
	$(MAKE) -C mod-bridging-stp-sysfs/src all
	$(MAKE) -C mod-bridging-stp-mstpd/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C odl clean
	$(MAKE) -C mod-bridging-uci/src clean
	$(MAKE) -C mod-bridging-ioctl/src clean
	$(MAKE) -C mod-bridging-dummy/src clean
	$(MAKE) -C mod-vlan-dummy/src clean
	$(MAKE) -C mod-bridging-stp-sysfs/src clean
	$(MAKE) -C mod-bridging-stp-mstpd/src clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-bridging-uci.so $(DEST)/usr/lib/amx/bridging/modules/mod-bridging-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-bridging-ioctl.so $(DEST)/usr/lib/amx/bridging/modules/mod-bridging-ioctl.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-stp-sysfs.so $(DEST)/usr/lib/amx/bridging/modules/mod-stp-sysfs.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-stp-mstpd.so $(DEST)/usr/lib/amx/bridging/modules/mod-stp-mstpd.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-bridging-dummy.so $(DEST)/usr/lib/amx/bridging/modules/mod-bridging-dummy.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-vlan-dummy.so $(DEST)/usr/lib/amx/bridging/modules/mod-vlan-dummy.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-bridging_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_port.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_port.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vlan.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_vlan.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vlanport.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_vlanport.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_stp.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_stp.odl
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(DEST)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/debug_bridge.sh $(DEST)/usr/lib/debuginfo/debug_bridge.sh

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-bridging-uci.so $(PKGDIR)/usr/lib/amx/bridging/modules/mod-bridging-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-bridging-ioctl.so $(PKGDIR)/usr/lib/amx/bridging/modules/mod-bridging-ioctl.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-stp-sysfs.so $(PKGDIR)/usr/lib/amx/bridging/modules/mod-stp-sysfs.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-stp-mstpd.so $(PKGDIR)/usr/lib/amx/bridging/modules/mod-stp-mstpd.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-bridging-dummy.so $(PKGDIR)/usr/lib/amx/bridging/modules/mod-bridging-dummy.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-vlan-dummy.so $(PKGDIR)/usr/lib/amx/bridging/modules/mod-vlan-dummy.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-bridging_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_port.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_port.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vlan.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_vlan.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vlanport.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_vlanport.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_stp.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_stp.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/debug_bridge.sh $(PKGDIR)/usr/lib/debuginfo/debug_bridge.sh
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/$(COMPONENT).odl)
	$(eval ODLFILES += odl/$(COMPONENT)_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C mod-bridging-uci/test run
	$(MAKE) -C mod-bridging-uci/test coverage
	$(MAKE) -C mod-bridging-ioctl/test run
	$(MAKE) -C mod-bridging-ioctl/test coverage
	$(MAKE) -C mod-bridging-stp-sysfs/test run
	$(MAKE) -C mod-bridging-stp-sysfs/test coverage
	$(MAKE) -C mod-bridging-stp-mstpd/test run
	$(MAKE) -C mod-bridging-stp-mstpd/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test