/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "../common/common_functions.h"
#include "../common/mock.h"

#include "bridging.h"
#include "bridging_utils.h"
#include "bridging_dm_utils.h"
#include "bridging_soc_utils.h"
#include "bridging_subscription_utils.h"

#include "dm_bridging_bridge.h"
#include "dm_bridging_port.h"
#include "dm_bridging_vlan.h"

#include "test_utils.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static const char* odl_config = "mock.odl";
static const char* odl_defs = "../../odl/tr181-bridging_definition.odl";
static const char* odl_mock_netmodel = "../common/mock_netmodel.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);
    //
    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netmodel, root_obj), 0);

    _bridge_main(AMXO_START, &dm, &parser);

    handle_events();

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    _bridge_main(AMXO_STOP, &dm, &parser);
    handle_events();

    handle_events();

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_bridge_info_new(UNUSED void** state) {
    int rv = -1;
    bridge_info_t* bridge = NULL;

    rv = bridge_info_clean(&bridge);
    assert_int_equal(rv, amxd_status_ok);

    rv = bridge_info_new(NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = bridge_info_new(&bridge);
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(bridge);

    rv = bridge_info_clean(NULL);
    assert_int_equal(rv, amxd_status_ok);

    rv = bridge_info_clean(&bridge);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(bridge);
}

void test_port_info_new(UNUSED void** state) {
    amxd_status_t rv = -1;
    port_info_t* port = NULL;
    amxd_object_t bridge;

    rv = port_info_clean(&port);
    assert_int_equal(rv, amxd_status_ok);

    rv = port_info_new(NULL, NULL, "test");
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = port_info_new(&port, NULL, NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = port_info_new(&port, &bridge, NULL);
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(port);

    rv = port_info_clean(&port);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(port);

    rv = port_info_new(&port, &bridge, "test");
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(port);

    rv = port_info_clean(NULL);
    assert_int_equal(rv, amxd_status_ok);

    rv = port_info_clean(&port);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(port);
}

void test_vlanport_info_new(UNUSED void** state) {
    amxd_status_t rv = -1;
    vlanport_info_t* vlanport = NULL;
    // This test just uses the root object for all object, any object will do for this test
    amxd_object_t* vlanport_obj = amxd_dm_get_root(&dm);
    amxd_object_t* bridge_obj = amxd_dm_get_root(&dm);

    // test cleaning of an uninitialized vlanport
    rv = vlanport_info_clean(&vlanport);
    assert_int_equal(rv, amxd_status_ok);

    // vlanport, vlanport_object and bridge_obj are mandatory arguments
    rv = vlanport_info_new(NULL, vlanport_obj, bridge_obj, NULL, NULL, NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = vlanport_info_new(&vlanport, NULL, bridge_obj, NULL, NULL, NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = vlanport_info_new(&vlanport, vlanport_obj, NULL, NULL, NULL, NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    // vlanport_name is allowed to be NULL
    rv = vlanport_info_new(&vlanport, vlanport_obj, bridge_obj, NULL, NULL, NULL);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(vlanport->vlanport_name);
    assert_non_null(vlanport);

    rv = vlanport_info_clean(&vlanport);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(vlanport);

    // vlanport_name is allowed to be empty
    rv = vlanport_info_new(&vlanport, vlanport_obj, bridge_obj, NULL, NULL, "");
    assert_int_equal(rv, amxd_status_ok);
    assert_null(vlanport->vlanport_name);
    assert_non_null(vlanport);

    rv = vlanport_info_clean(&vlanport);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(vlanport);

    // set and actual vlanport_name
    rv = vlanport_info_new(&vlanport, vlanport_obj, bridge_obj, NULL, NULL, "vlantest");
    assert_int_equal(rv, amxd_status_ok);
    assert_string_equal(vlanport->vlanport_name, "vlantest");
    assert_non_null(vlanport);

    // Cleaning NULL should not be a problem
    rv = vlanport_info_clean(NULL);
    assert_int_equal(rv, amxd_status_ok);

    // actual cleanup of this test
    rv = vlanport_info_clean(&vlanport);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(vlanport);
}

void test_bridging_update_port_status_invalid_arg(UNUSED void** state) {
    amxd_status_t rv = amxd_status_unknown_error;
    port_info_t* port = NULL;
    amxd_object_t bridge;

    // port can not be NULL
    rv = bridging_update_port_status(NULL, "Error");
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    // port->port_obj can not be NULL
    rv = bridging_update_port_status(port, "Error");
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    // create a valid port
    rv = port_info_new(&port, &bridge, "eth0");
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(port);
    port->port_obj = amxd_dm_findf(&dm, "Bridging.Bridge.lan.Port.ETH0.");
    assert_non_null(port->port_obj);

    // new_status can not be NULL
    rv = bridging_update_port_status(port, NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    // new_status can not be empty
    rv = bridging_update_port_status(port, "");
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    port_info_clean(&port);
}

void test_bridging_update_bridge_status(UNUSED void** state) {
    amxd_status_t rv = -1;
    amxd_object_t* bridge_obj;
    char* bridge_status = NULL;

    bridge_obj = amxd_dm_findf(&dm, "Bridging.Bridge.lan.");
    assert_non_null(bridge_obj);

    rv = bridging_update_bridge_status(bridge_obj, BRIDGE_DISABLED);
    assert_int_equal(rv, amxd_status_ok);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, BRIDGE_STATUS_DISABLED);
    free(bridge_status);

    rv = bridging_update_bridge_status(bridge_obj, BRIDGE_ENABLED);
    assert_int_equal(rv, amxd_status_ok);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, BRIDGE_STATUS_ENABLED);
    free(bridge_status);

    rv = bridging_update_bridge_status(bridge_obj, BRIDGE_ERROR);
    assert_int_equal(rv, amxd_status_ok);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, BRIDGE_STATUS_ERROR);
    free(bridge_status);

    rv = bridging_update_bridge_status(bridge_obj, 99);
    assert_int_equal(rv, amxd_status_ok);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, BRIDGE_STATUS_DISABLED);
    free(bridge_status);

    rv = bridging_update_bridge_status(amxd_object_get_parent(bridge_obj), 99);
    assert_int_equal(rv, amxd_status_parameter_not_found);
}

void test_bridging_update_port_status(UNUSED void** state) {
    amxd_status_t rv = -1;
    port_info_t* port;
    amxd_object_t bridge;
    char* intf_status = NULL;
    char input_states[8][20] = {"notpresent", "down", "lowerlayerdown", "dormant", "up", "Error",
        "unknown", "invalid_state"};
    char output_states[8][20] = {"NotPresent", "Down", "LowerLayerDown", "Dormant", "Up", "Error",
        "Unknown", "Unknown"};

    rv = port_info_new(&port, &bridge, "test");
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(port);
    port->port_obj = amxd_dm_findf(&dm, "Bridging.Bridge.lan.Port.ETH0.");
    assert_non_null(port->port_obj);
    for(int i = 0; i < 8; i++) {

        rv = bridging_update_port_status(port, input_states[i]);
        assert_int_equal(rv, amxd_status_ok);
        handle_events();

        intf_status = amxd_object_get_value(cstring_t, port->port_obj, "Status", NULL);
        assert_int_equal(strcmp(output_states[i], intf_status), 0);
        free(intf_status);
    }
    port_info_clean(&port);
    assert_int_equal(0, 0);
}

void test_port_type(UNUSED void** state) {
    amxd_status_t rv = -1;
    port_info_t* port;
    amxd_object_t bridge;

    rv = port_info_new(&port, &bridge, "test");
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(port);

    port_type(port, "ProviderNetworkPort");
    assert_int_equal(port->port_type, PROVIDER_NETWORK_PORT);

    port_type(port, "CustomerNetworkPort");
    assert_int_equal(port->port_type, CUSTOMER_NETWORK_PORT);

    port_type(port, "CustomerEdgePort");
    assert_int_equal(port->port_type, CUSTOMER_EDGE_PORT);

    port_type(port, "CustomerVLANPort");
    assert_int_equal(port->port_type, CUSTOMER_VLAN_PORT);

    port_type(port, "VLANUnawarePort");
    assert_int_equal(port->port_type, VLAN_UNAWARE_PORT);

    port_type(port, "not_a_type");
    assert_int_equal(port->port_type, UNKNOWN_PORT_TYPE);

    port_info_clean(&port);
}
