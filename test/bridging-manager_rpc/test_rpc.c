/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb_register.h>
#include <amxut/amxut_bus.h>

#include "../common/common_functions.h"

#include "bridging.h"
#include "bridging_utils.h"

#include "dm_bridging_bridge.h"
#include "dm_bridging_port.h"
#include "dm_bridging_vlan.h"

#include "../common/mock.h"
#include "test_rpc.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

static const char* odl_config = "mock.odl";
static const char* odl_defs = "../../odl/tr181-bridging_definition.odl";
static const char* odl_mock_netdev = "mock_netdev_dm.odl";
static const char* odl_mock_netmodel = "../common/mock_netmodel.odl";

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root = NULL;

    amxut_bus_setup(NULL);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    root = amxd_dm_get_root(dm);
    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(root);

    resolver_add_all_functions(parser);

    assert_int_equal(amxo_parser_parse_file(parser, odl_mock_netdev, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, odl_mock_netmodel, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, odl_config, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, odl_defs, root), 0);

    assert_int_equal(_bridge_main(AMXO_START, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();

    return 0;
}

int test_dm_teardown(void** state) {
    assert_int_equal(_bridge_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();

    amxut_bus_teardown(state);

    return 0;
}

void test_add_port_requires_ll(UNUSED void** state) {
    int rv = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Bridging.");
    amxc_var_t params;
    amxc_var_t ret;

    amxc_var_init(&params);
    amxc_var_init(&ret);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "AddPort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    // If only the LowerLayers value is missing, function should still fail
    amxc_var_add_key(cstring_t, &params, "Alias", "test_port");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(uint32_t, &params, "VlanId", 100);
    amxc_var_add_key(cstring_t, &params, "VlanName", "VLAN100");
    amxc_var_add_key(uint32_t, &params, "VlanPriority", 3);
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "AddPort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_invalid_function_argument);
    amxut_bus_handle_events();

    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_add_port(UNUSED void** state) {
    int rv = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Bridging.");
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t port_params;
    amxd_object_t* port_obj = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_init(&port_params);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&port_params, AMXC_VAR_ID_HTABLE);

    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    assert_null(port_obj);

    amxc_var_add_key(cstring_t, &params, "LowerLayers", "Device.Ethernet.Interface.1");
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "AddPort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_ok);
    amxut_bus_handle_events();

    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    assert_non_null(port_obj);

    amxd_object_get_params(port_obj, &port_params, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&port_params, "Alias"), "cpe-Port-2"); // Should be a auto generated name
    assert_string_equal(GET_CHAR(&port_params, "Type"), "VLANUnawarePort");
    assert_false(GET_BOOL(&port_params, "Enable"));                     // If Enable is not set, port should be disabled
    assert_false(GET_BOOL(&port_params, "ManagementPort"));

    // Calling the function again with the same LL but enable set to true should enable the existing port
    amxc_var_add_key(cstring_t, &params, "LowerLayers", "Device.Ethernet.Interface.1");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Alias", "test_port"); // Providing an Alias should not effect this
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "AddPort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_ok);
    amxut_bus_handle_events();

    assert_true(amxd_object_get_value(bool, port_obj, "Enable", NULL));

    amxc_var_clean(&port_params);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
    // Teardown is not called after this test, continues in test_disable_port
}

void test_disable_port(UNUSED void** state) {
    // This test continues on test_add_port, no setup is called for this tests
    int rv = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Bridging.");
    amxc_var_t params;
    amxc_var_t ret;
    amxd_object_t* port_obj = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    assert_non_null(port_obj);

    amxc_var_add_key(cstring_t, &params, "LowerLayers", "Device.Ethernet.Interface.1");
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "DisablePort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_ok);
    amxut_bus_handle_events();

    // Port instance should be disabled
    assert_false(amxd_object_get_value(bool, port_obj, "Enable", NULL));

    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_add_aliased_port(UNUSED void** state) {
    int rv = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Bridging.");
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t port_params;
    amxd_object_t* port_obj = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_init(&port_params);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&port_params, AMXC_VAR_ID_HTABLE);

    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    assert_null(port_obj);

    amxc_var_add_key(cstring_t, &params, "LowerLayers", "Device.Ethernet.Interface.1");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Alias", "test_port");
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "AddPort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_ok);
    amxut_bus_handle_events();

    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    assert_non_null(port_obj);

    amxd_object_get_params(port_obj, &port_params, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&port_params, "Alias"), "test_port"); // Should be the requested name
    assert_string_equal(GET_CHAR(&port_params, "Type"), "VLANUnawarePort");
    assert_true(GET_BOOL(&port_params, "Enable"));
    assert_false(GET_BOOL(&port_params, "ManagementPort"));

    amxc_var_clean(&port_params);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_add_vlan_port(UNUSED void** state) {
    int rv = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Bridging.");
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t obj_params;
    amxd_object_t* bridge_obj = NULL;
    amxd_object_t* port_obj = NULL;
    amxd_object_t* vlan_obj = NULL;
    amxd_object_t* vlanport_obj = NULL;
    char* standard = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    bridge_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.");
    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    vlan_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLAN.1.");
    vlanport_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLANPort.1.");

    assert_non_null(bridge_obj);
    assert_null(port_obj);
    assert_null(vlan_obj);
    assert_null(vlanport_obj);

    // Check initial bridge Standard
    standard = amxd_object_get_value(cstring_t, bridge_obj, "Standard", NULL);
    assert_string_equal(standard, "802.1D-2004");
    free(standard);

    amxc_var_add_key(cstring_t, &params, "LowerLayers", "Device.Ethernet.Interface.1");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Alias", "test_port");
    amxc_var_add_key(uint32_t, &params, "VlanId", 100);
    amxc_var_add_key(uint32_t, &params, "VlanPriority", 4);
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "AddPort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_ok);
    amxut_bus_handle_events();

    // Check bridge Standard
    standard = amxd_object_get_value(cstring_t, bridge_obj, "Standard", NULL);
    assert_string_equal(standard, "802.1Q-2011");
    free(standard);

    // Check the port parameters
    amxc_var_init(&obj_params);
    amxc_var_set_type(&obj_params, AMXC_VAR_ID_HTABLE);
    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    assert_non_null(port_obj);
    amxd_object_get_params(port_obj, &obj_params, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&obj_params, "Alias"), "test_port"); // Should be the requested name
    assert_string_equal(GET_CHAR(&obj_params, "Type"), "CustomerVLANPort");
    assert_int_equal(GET_UINT32(&obj_params, "DefaultUserPriority"), 0);
    assert_true(GET_BOOL(&obj_params, "Enable"));
    assert_false(GET_BOOL(&obj_params, "ManagementPort"));
    amxc_var_clean(&obj_params);

    // check the VLAN parameters
    amxc_var_init(&obj_params);
    amxc_var_set_type(&obj_params, AMXC_VAR_ID_HTABLE);
    vlan_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLAN.1.");
    assert_non_null(vlan_obj);
    amxd_object_get_params(vlan_obj, &obj_params, amxd_dm_access_protected);
    assert_int_equal(GET_UINT32(&obj_params, "VLANID"), 100);
    assert_true(GET_BOOL(&obj_params, "Enable"));
    amxc_var_clean(&obj_params);

    // Check the VLANPort parameters
    amxc_var_init(&obj_params);
    amxc_var_set_type(&obj_params, AMXC_VAR_ID_HTABLE);
    vlanport_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLANPort.1.");
    assert_non_null(vlanport_obj);
    amxd_object_get_params(vlanport_obj, &obj_params, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&obj_params, "Port"), "Device.Bridging.Bridge.1.Port.2");
    assert_string_equal(GET_CHAR(&obj_params, "VLAN"), "Device.Bridging.Bridge.1.VLAN.1");
    assert_string_equal(GET_CHAR(&obj_params, "Name"), "");
    assert_int_equal(GET_UINT32(&obj_params, "VlanPriority"), 4);
    assert_true(GET_BOOL(&obj_params, "Enable"));
    amxc_var_clean(&obj_params);

    amxc_var_clean(&obj_params);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
    // Teardown is not called after this test, continues in test_add_second_vlan_port
}

void test_add_second_vlan_port(UNUSED void** state) {
    // This test continues on test_add_vlan_port, no setup is called for this tests
    int rv = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Bridging.");
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t obj_params;
    amxd_object_t* port_obj = NULL;
    amxd_object_t* vlan_obj = NULL;
    amxd_object_t* vlanport_obj = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    vlan_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLAN.2.");
    vlanport_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLANPort.2.");
    assert_non_null(port_obj);
    assert_null(vlan_obj);
    assert_null(vlanport_obj);

    amxc_var_add_key(cstring_t, &params, "LowerLayers", "Device.Ethernet.Interface.1");
    amxc_var_add_key(bool, &params, "Enable", true);
    amxc_var_add_key(cstring_t, &params, "Alias", "test_port");
    amxc_var_add_key(uint32_t, &params, "VlanId", 200);
    amxc_var_add_key(uint32_t, &params, "VlanPriority", 4);
    amxc_var_add_key(cstring_t, &params, "VlanName", "VLAN200");
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "AddPort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_ok);
    amxut_bus_handle_events();

    // Check the port parameters, should reuse the previous one
    amxc_var_init(&obj_params);
    amxc_var_set_type(&obj_params, AMXC_VAR_ID_HTABLE);
    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    assert_non_null(port_obj);
    amxd_object_get_params(port_obj, &obj_params, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&obj_params, "Alias"), "test_port"); // Should be the requested name
    assert_string_equal(GET_CHAR(&obj_params, "Type"), "CustomerVLANPort");
    assert_int_equal(GET_UINT32(&obj_params, "DefaultUserPriority"), 0);
    assert_true(GET_BOOL(&obj_params, "Enable"));
    assert_false(GET_BOOL(&obj_params, "ManagementPort"));
    amxc_var_clean(&obj_params);

    // check the VLAN parameters, a new instance should be added
    amxc_var_init(&obj_params);
    amxc_var_set_type(&obj_params, AMXC_VAR_ID_HTABLE);
    vlan_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLAN.2.");
    assert_non_null(vlan_obj);
    amxd_object_get_params(vlan_obj, &obj_params, amxd_dm_access_protected);
    assert_int_equal(GET_UINT32(&obj_params, "VLANID"), 200);
    assert_true(GET_BOOL(&obj_params, "Enable"));
    amxc_var_clean(&obj_params);

    // Check the VLANPort parameters, a new instance should be added
    amxc_var_init(&obj_params);
    amxc_var_set_type(&obj_params, AMXC_VAR_ID_HTABLE);
    vlanport_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLANPort.2.");
    assert_non_null(vlanport_obj);
    amxd_object_get_params(vlanport_obj, &obj_params, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&obj_params, "Port"), "Device.Bridging.Bridge.1.Port.2");
    assert_string_equal(GET_CHAR(&obj_params, "VLAN"), "Device.Bridging.Bridge.1.VLAN.2");
    assert_string_equal(GET_CHAR(&obj_params, "Name"), "VLAN200");
    assert_int_equal(GET_UINT32(&obj_params, "VlanPriority"), 4);
    assert_true(GET_BOOL(&obj_params, "Enable"));
    amxc_var_clean(&obj_params);

    amxc_var_clean(&obj_params);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
    // Teardown is not called after this test, continues in test_disable_vlan_port
}

void test_disable_vlan_port(UNUSED void** state) {
    // This test continues on test_add_second_vlan_port, no setup is called for this tests
    int rv = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Bridging.");
    amxc_var_t params;
    amxc_var_t ret;
    amxd_object_t* port_obj = NULL;
    amxd_object_t* vlan_obj = NULL;
    amxd_object_t* vlanport_obj = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    port_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.Port.[LowerLayers == 'Device.Ethernet.Interface.1'].");
    vlan_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLAN.2.");
    vlanport_obj = amxd_dm_findf(dm, "Bridging.Bridge.1.VLANPort.2.");
    assert_non_null(port_obj);
    assert_non_null(vlan_obj);
    assert_non_null(vlanport_obj);

    amxc_var_add_key(cstring_t, &params, "LowerLayers", "Device.Ethernet.Interface.1");
    amxc_var_add_key(uint32_t, &params, "VlanId", 200);
    rv = amxb_call(bus_ctx, "Bridging.Bridge.1.", "DisablePort", &params, &ret, 5);
    assert_int_equal(rv, amxd_status_ok);
    amxut_bus_handle_events();

    // Only the vlanport instance should be disabled
    assert_true(amxd_object_get_value(bool, port_obj, "Enable", NULL));
    assert_true(amxd_object_get_value(bool, vlan_obj, "Enable", NULL));
    assert_false(amxd_object_get_value(bool, vlanport_obj, "Enable", NULL));

    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}
