/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "bridging.h"
#include "bridging_utils.h"

#include "dm_bridging_bridge.h"
#include "dm_bridging_port.h"
#include "dm_bridging_vlan.h"
#include "dm_bridging_stp.h"
#include "bridge_rpc.h"

#include "../common/common_functions.h"
#include "../common/mock.h"

static const char* value = "";

static amxd_status_t _dummy_func(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static amxd_status_t _dummy_resolvePath(UNUSED amxd_object_t* object,
                                        UNUSED amxd_function_t* func,
                                        UNUSED amxc_var_t* args,
                                        amxc_var_t* ret) {
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, ret, "resolver");

    return amxd_status_ok;
}

static amxd_status_t _dummy_getResult(UNUSED amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      amxc_var_t* ret) {
    amxc_var_set(cstring_t, ret, value);

    return amxd_status_ok;
}

static amxd_status_t _dummy_openQuery(UNUSED amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      amxc_var_t* ret) {
    amxc_var_set(uint32_t, ret, 1);

    return amxd_status_ok;
}

static amxd_status_t _dummy_closeQuery(UNUSED amxd_object_t* object,
                                       UNUSED amxd_function_t* func,
                                       UNUSED amxc_var_t* args,
                                       UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static void dummy_resolve_netmodel_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "resolvePath",
                                            AMXO_FUNC(_dummy_resolvePath)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "openQuery",
                                            AMXO_FUNC(_dummy_openQuery)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "closeQuery",
                                            AMXO_FUNC(_dummy_closeQuery)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "getResult",
                                            AMXO_FUNC(_dummy_getResult)), 0);
}

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_check_management_port",
                                            AMXO_FUNC(_bridging_check_management_port)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_bridge_added",
                                            AMXO_FUNC(_bridging_bridge_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_bridge_changed",
                                            AMXO_FUNC(_bridging_bridge_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_bridge_removed",
                                            AMXO_FUNC(_bridging_bridge_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_managementport_changed",
                                            AMXO_FUNC(_bridging_managementport_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_added",
                                            AMXO_FUNC(_bridging_port_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_changed",
                                            AMXO_FUNC(_bridging_port_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_removed",
                                            AMXO_FUNC(_bridging_port_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_deleted",
                                            AMXO_FUNC(_bridging_port_deleted)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "lastchange_on_read",
                                            AMXO_FUNC(_lastchange_on_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_is_empty_or_in",
                                            AMXO_FUNC(_check_is_empty_or_in)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_max_bridge_entries",
                                            AMXO_FUNC(_check_max_bridge_entries)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_max_qbridge_entries",
                                            AMXO_FUNC(_check_max_qbridge_entries)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_max_dbridge_entries",
                                            AMXO_FUNC(_check_max_dbridge_entries)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_max_std_entries",
                                            AMXO_FUNC(_check_max_std_entries)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_type_changed",
                                            AMXO_FUNC(_bridging_port_type_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_default_priority_changed",
                                            AMXO_FUNC(_bridging_port_default_priority_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_lower_layers_changed",
                                            AMXO_FUNC(_bridging_port_lower_layers_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_port_not_implemented",
                                            AMXO_FUNC(_bridging_port_not_implemented)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlanport_added",
                                            AMXO_FUNC(_bridging_vlanport_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlanport_enable_changed",
                                            AMXO_FUNC(_bridging_vlanport_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlanport_changed",
                                            AMXO_FUNC(_bridging_vlanport_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlanport_renamed",
                                            AMXO_FUNC(_bridging_vlanport_renamed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlanport_priority_changed",
                                            AMXO_FUNC(_bridging_vlanport_priority_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlan_deleted",
                                            AMXO_FUNC(_bridging_vlan_deleted)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlan_id_changed",
                                            AMXO_FUNC(_bridging_vlan_id_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlanport_removed",
                                            AMXO_FUNC(_bridging_vlanport_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_vlan_removed",
                                            AMXO_FUNC(_bridging_vlan_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_stp_changed",
                                            AMXO_FUNC(_bridging_stp_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "bridging_agingtime_changed",
                                            AMXO_FUNC(_bridging_agingtime_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_bridge_prio",
                                            AMXO_FUNC(_is_valid_bridge_prio)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "AddPort",
                                            AMXO_FUNC(_AddPort)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "DisablePort",
                                            AMXO_FUNC(_DisablePort)), 0);

    // resolve external functions to a dummy function
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_read",
                                            AMXO_FUNC(_dummy_func)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_list",
                                            AMXO_FUNC(_dummy_func)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_describe",
                                            AMXO_FUNC(_dummy_func)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "matches_regexp",
                                            AMXO_FUNC(_dummy_func)), 0);
    dummy_resolve_netmodel_functions(parser);
}

void set_query_getResult_value(const char* result) {
    value = result;
}
