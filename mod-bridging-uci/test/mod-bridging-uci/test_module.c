/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../common/mock.h"
#include "test_module.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

#define DUMMY_TEST_ODL "../common/dummy.odl"
#define UCI_ADD "uci_add"
#define UCI_DELETE "uci_delete"
#define UCI_SET "uci_set"
#define UCI_GET "uci_get"
#define UCI_COMMIT "uci_commit"

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

// read sig alarm is used to wait for a timer to run out so the callback
// function for this timer will be executed before continuing with the
// next test. In these tests it is used to wait on the delayed uci commit
static void read_sig_alarm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        amxp_timers_calculate();
        amxp_timers_check();
        handle_events();
    }
}


int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    amxo_resolver_ftab_add(&parser, "get", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(&parser, "commit", AMXO_FUNC(dummy_function_commit));
    amxo_resolver_ftab_add(&parser, "set", AMXO_FUNC(dummy_function_set));
    amxo_resolver_ftab_add(&parser, "delete", AMXO_FUNC(dummy_function_del));
    amxo_resolver_ftab_add(&parser, "add", AMXO_FUNC(dummy_function_add));

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, DUMMY_TEST_ODL, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    _dummy_main(0, &dm, &parser);

    handle_events();

    dummy_set_file(UCI_ADD, "../common/data/uci_add_network_interface_lan.json", "network_interface_lan");
    dummy_set_file(UCI_COMMIT, "../common/data/uci_commit.json", "uci_commit");
    dummy_set_file(UCI_DELETE, "../common/data/uci_delete_network_lan.json", "network_lan");
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_lan.json", "network_lan");
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_interface.json", "network_interface");
    dummy_set_file(UCI_GET, "../common/data/uci_get_wireless_wifi-iface.json", "wireless_wifi-iface");
    dummy_set_file(UCI_SET, "../common/data/uci_set_network_lan.json", "network_lan");
    dummy_set_file(UCI_SET, "../common/data/uci_set_wireless_wifi-iface_default_wl0.json", "wireless_wifi-iface_default_wl0");
    dummy_set_file(UCI_SET, "../common/data/uci_set_wireless_wifi-iface_default_wl1.json", "wireless_wifi-iface_default_wl1");
    dummy_set_file(UCI_SET, "../common/data/uci_set_network_guest.json", "network_guest");

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
}

void test_add_bridge(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;

    amxc_var_init(&ret);
    data = dummy_read_json_from_file("test_data/test_bridge.json");
    assert_non_null(data);

    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "add-bridge", data, &ret), 0);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_add_port(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t get_netw;
    amxc_var_t check;
    amxc_var_t* data;
    amxc_var_t* values;
    amxc_var_init(&ret);
    amxc_var_init(&get_netw);
    amxc_var_init(&check);

    amxc_var_set_type(&get_netw, AMXC_VAR_ID_HTABLE);
    dummy_write_json_to_file("../common/data/uci_get_network_lan.json", &get_netw);

    amxc_var_set_type(&check, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check, "ifname", "eth3");
    dummy_write_json_to_file("../common/data/uci_set_network_lan.json", &check);

    // try to add a management port
    data = dummy_read_json_from_file("test_data/test_management_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "add-port", data, &ret), 0);
    amxc_var_delete(&data);

    // add port(eth3) to network config
    data = dummy_read_json_from_file("test_data/test_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "add-port", data, &ret), 0);
    amxc_var_delete(&data);

    // add port(wl0) to wireless config
    data = dummy_read_json_from_file("test_data/test_wireless_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "add-port", data, &ret), 0);
    amxc_var_delete(&data);

    // change the uci_get_network_lan file to simulate a port that already has ports
    amxc_var_set_type(&get_netw, AMXC_VAR_ID_HTABLE);
    values = amxc_var_add_key(amxc_htable_t, &get_netw, "values", NULL);
    amxc_var_add_key(int8_t, values, ".anonymous", 0);
    amxc_var_add_key(int8_t, values, ".index", 7);
    amxc_var_add_key(cstring_t, values, ".name", "lan");
    amxc_var_add_key(cstring_t, values, "ifname", "eth1 eth2");
    amxc_var_add_key(cstring_t, values, "type", "bridge");

    dummy_write_json_to_file("../common/data/uci_get_network_lan.json", &get_netw);

    // change the expected result
    amxc_var_clean(&check);
    amxc_var_set_type(&check, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check, "ifname", "eth1 eth2 eth3");
    dummy_write_json_to_file("../common/data/uci_set_network_lan.json", &check);

    // add port(eth3) to a bridge that already has ports
    data = dummy_read_json_from_file("test_data/test_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "add-port", data, &ret), 0);
    amxc_var_delete(&data);

    // change the uci_get_network_lan file to simulate a port that already has eth3
    amxc_var_set_type(&get_netw, AMXC_VAR_ID_HTABLE);
    values = amxc_var_add_key(amxc_htable_t, &get_netw, "values", NULL);
    amxc_var_add_key(int8_t, values, ".anonymous", 0);
    amxc_var_add_key(int8_t, values, ".index", 7);
    amxc_var_add_key(cstring_t, values, ".name", "lan");
    amxc_var_add_key(cstring_t, values, "ifname", "eth1 eth2 eth3");
    amxc_var_add_key(cstring_t, values, "type", "bridge");

    dummy_write_json_to_file("../common/data/uci_get_network_lan.json", &get_netw);
    // add port(eth3) to a bridge that already has this ports
    data = dummy_read_json_from_file("test_data/test_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "add-port", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_clean(&ret);
    amxc_var_clean(&get_netw);
    amxc_var_clean(&check);
}

void test_remove_port(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t get_netw;
    amxc_var_t check;
    amxc_var_t* data;
    amxc_var_t* values;
    amxc_var_init(&ret);
    amxc_var_init(&get_netw);
    amxc_var_init(&check);

    amxc_var_set_type(&get_netw, AMXC_VAR_ID_HTABLE);
    values = amxc_var_add_key(amxc_htable_t, &get_netw, "values", NULL);
    amxc_var_add_key(int8_t, values, ".anonymous", 0);
    amxc_var_add_key(int8_t, values, ".index", 7);
    amxc_var_add_key(cstring_t, values, ".name", "lan");
    amxc_var_add_key(cstring_t, values, "ifname", "eth1 eth2 eth3");
    amxc_var_add_key(cstring_t, values, "type", "bridge");

    dummy_write_json_to_file("../common/data/uci_get_network_lan.json", &get_netw);

    amxc_var_set_type(&check, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check, "ifname", "eth1 eth2");
    dummy_write_json_to_file("../common/data/uci_set_network_lan.json", &check);

    // removing management port should not do anything since it should not be in the ifname
    data = dummy_read_json_from_file("test_data/test_management_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "remove-port", data, &ret), 0);
    amxc_var_delete(&data);

    // remove port from network config
    data = dummy_read_json_from_file("test_data/test_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "remove-port", data, &ret), 0);
    amxc_var_delete(&data);

    // try to remove wireless port config, should return ok without doing anything
    data = dummy_read_json_from_file("test_data/test_remove_wireless_port.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "remove-port", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_clean(&ret);
    amxc_var_clean(&get_netw);
    amxc_var_clean(&check);
}

void test_disable_bridge(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;

    amxc_var_init(&ret);
    data = dummy_read_json_from_file("test_data/test_guest_bridge.json");
    assert_non_null(data);

    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "disable-bridge", data, &ret), 0);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_enable_bridge(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;

    amxc_var_init(&ret);
    dummy_set_file(UCI_SET, "../common/data/uci_set_network_guest_enabled.json", "network_guest");
    data = dummy_read_json_from_file("test_data/test_guest_bridge.json");
    assert_non_null(data);

    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "enable-bridge", data, &ret), 0);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_remove_bridge(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;

    amxc_var_init(&ret);
    data = dummy_read_json_from_file("test_data/test_bridge.json");
    assert_non_null(data);

    assert_int_equal(amxm_execute_function("target_module", "bridge-ctrl", "remove-bridge", data, &ret), 0);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}
