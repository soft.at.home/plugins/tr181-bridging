# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.8.16 - 2024-12-11(15:13:20 +0000)

### Other

- bridge is not working even after DM is enabled

## Release v2.8.15 - 2024-11-28(13:14:06 +0000)

### Other

- STP must be enabled on both HGW and Repeater

## Release v2.8.14 - 2024-10-11(14:07:58 +0000)

### Other

- - getDebug - Add some brctl commands

## Release v2.8.13 - 2024-10-02(11:58:39 +0000)

### Other

- - [Terminating dot][TR181] Fix Bridging Component

## Release v2.8.12 - 2024-09-30(06:53:43 +0000)

### Other

- - [Terminating dot][TR181] Fix Bridging Component

## Release v2.8.11 - 2024-09-27(08:57:45 +0000)

### Other

- [Netdev] Create/destroy in loop eth5.10/eth5_10 interface

## Release v2.8.10 - 2024-09-26(11:14:58 +0000)

### Other

- - [Terminating dot][TR181] Fix Bridging Component

## Release v2.8.9 - 2024-09-10(07:18:49 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v2.8.8 - 2024-09-05(07:41:31 +0000)

### Other

- [NetModel] Connect via direct socket if possible

## Release v2.8.7 - 2024-09-05(06:59:57 +0000)

### Other

- [TR181-bridging] STP must be enabled on both HGW and Repeater

## Release v2.8.6 - 2024-07-23(07:45:37 +0000)

### Fixes

- Better shutdown script

## Release v2.8.5 - 2024-06-17(15:27:23 +0000)

### Other

- failed to initialize libnetmodel

## Release v2.8.4 - 2024-05-06(07:10:08 +0000)

### Other

- Data model parameters needed for IGMPs in Routing Page

## Release v2.8.3 - 2024-04-22(12:33:47 +0000)

### Changes

- avoid '.' in vlanport interfaces

## Release v2.8.2 - 2024-04-10(10:03:42 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.8.1 - 2024-04-04(11:34:31 +0000)

### Fixes

- bridge state DOWN on first boot

## Release v2.8.0 - 2024-03-23(13:07:31 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.7.1 - 2024-03-22(14:22:18 +0000)

### Fixes

- fix wrong lowerlayer value for moca interfaces in ucode template

## Release v2.7.0 - 2024-03-08(11:32:08 +0000)

### New

- Add support for wds interfaces to default template

## Release v2.6.0 - 2024-03-05(09:39:52 +0000)

### New

- Extend ucode template to support untagged and tagged traffic in the same bridge
- Extend ucode template to allow setting VlanPriority in networklayout.json

### Fixes

- Take bridge index into account when generating VLANPort object using ucode

## Release v2.5.0 - 2024-02-19(07:48:21 +0000)

### New

- Extend ucode template to add support for vlans on the bridge

## Release v2.4.0 - 2024-02-08(16:15:50 +0000)

### New

- Add bridges dynamically based on networklayout.json

## Release v2.3.7 - 2024-01-10(12:20:22 +0000)

### Fixes

- DefaultUserPriority is defined per Port and will be overwritten

## Release v2.3.6 - 2023-12-22(13:54:30 +0000)

### Other

- Changing LowerLayers on a disabled bridge port will add the...

## Release v2.3.5 - 2023-12-13(16:27:53 +0000)

## Release v2.3.4 - 2023-11-30(12:36:15 +0000)

### Fixes

- [Guest Bridge] Add guest support on AP, using ethernet backhaul.

## Release v2.3.3 - 2023-11-20(13:44:42 +0000)

### Other

- Set lowerlayer for moca interface

## Release v2.3.2 - 2023-11-14(08:20:48 +0000)

### Other

- Unit test rpc functions

## Release v2.3.1 - 2023-11-09(15:17:40 +0000)

### Fixes

- Resolve LowerLayers for CustomerVLANPorts

## Release v2.3.0 - 2023-10-23(13:49:19 +0000)

### New

- add a bridge WANMode in the WANManager

## Release v2.2.0 - 2023-10-13(14:11:50 +0000)

### New

- [amxrt][no-root-user][capability drop]bridging-manager must be adapted to run as non-root and lmited capabilities

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v2.1.0 - 2023-06-22(14:27:14 +0000)

### New

- [Bridging][STP] Add support for STP in the Bridging Model according to the tr181 v16

## Release v2.0.16 - 2023-06-10(07:30:23 +0000)

### Other

- add ACLs permissions for cwmp user

## Release v2.0.15 - 2023-05-11(10:00:05 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v2.0.14 - 2023-04-21(17:32:24 +0000)

### Fixes

- Handle a dot in the NetDev name

## Release v2.0.13 - 2023-04-05(16:30:09 +0000)

### Fixes

- tr181-bridging *.LastChanges are wrong in datamodel

## Release v2.0.12 - 2023-03-24(09:23:51 +0000)

### Other

- Adapt odl templates to define wifi lowerlayers

## Release v2.0.11 - 2023-03-22(08:39:03 +0000)

### Fixes

- Sometimes after a reboot the bridge is down

## Release v2.0.10 - 2023-03-20(11:40:57 +0000)

### Fixes

- Add in/out traces to all the functions

## Release v2.0.9 - 2023-03-17(18:40:50 +0000)

### Other

- [baf] Correct typo in config option

## Release v2.0.8 - 2023-03-16(13:49:21 +0000)

### Other

- Add AP config files

## Release v2.0.7 - 2023-03-10(10:51:39 +0000)

### Changes

- Enable LCM bridge by default

## Release v2.0.6 - 2023-03-09(09:19:11 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v2.0.5 - 2023-03-02(12:57:51 +0000)

### Other

- tr181-components: missing explicit dependency on rpcd service providing ubus uci backend
- Move odl templates to components

## Release v2.0.4 - 2023-02-23(09:45:43 +0000)

### Fixes

- When booting, the lan ip configuration will first appear on the eth1 interface instead of the bridge

## Release v2.0.3 - 2023-02-20(14:23:28 +0000)

### Fixes

- When booting, the lan ip configuration will first appear on the eth1 interface instead of the bridge

## Release v2.0.2 - 2023-01-26(14:14:58 +0000)

### Changes

- Adapt odl defaults to match for wnc

## Release v2.0.1 - 2023-01-23(16:16:20 +0000)

### Fixes

- Remove comma preventing bridging manager to start

## Release v2.0.0 - 2023-01-23(08:40:12 +0000)

### Removed

- Remove the loading of the UCI config

## Release v1.12.8 - 2023-01-12(10:28:47 +0000)

## Release v1.12.7 - 2023-01-09(10:15:57 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v1.12.6 - 2023-01-09(09:29:27 +0000)

### Fixes

- init without clean on local variables

## Release v1.12.5 - 2022-12-20(11:57:01 +0000)

### Fixes

- Vlans are not added correctly to the bridge

## Release v1.12.4 - 2022-12-09(09:15:35 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v1.12.3 - 2022-12-01(08:47:44 +0000)

### Other

- Disable import-dbg in plugins.

## Release v1.12.2 - 2022-11-17(07:16:33 +0000)

### Fixes

- Remove wireless parameters from wifi interfaces in the defaults

## Release v1.12.1 - 2022-11-07(09:40:28 +0000)

### Other

- [TR181][Bridging] revert using ioctl instead of uci [config]

## Release v1.12.0 - 2022-11-05(08:04:44 +0000)

### New

- [tr181][Bridging] add wifi ssid to the guest interfaces

## Release v1.11.2 - 2022-11-05(07:58:11 +0000)

### Fixes

- Bridges need to be disabled before being removed by the IOCTL module

## Release v1.11.1 - 2022-11-03(08:27:44 +0000)

### Other

- [TR181][Bridging] use ioctl instead of uci [config]

## Release v1.11.0 - 2022-10-06(08:32:08 +0000)

### New

- [tr181-bridgemanager] Fill in LowerLayers parameter for wifi interfaces

## Release v1.10.8 - 2022-07-14(09:01:46 +0000)

### Fixes

- Make lowerlayers parameter writable

## Release v1.10.7 - 2022-06-23(17:29:58 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v1.10.6 - 2022-05-19(12:32:17 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v1.10.5 - 2022-04-21(12:31:33 +0000)

### Changes

- Use netmodel Query to listen on LowerLayer updates

## Release v1.10.4 - 2022-04-19(12:03:17 +0000)

### Changes

- [Ethernet Manager] Vlan Interfaces are not up after creation

## Release v1.10.3 - 2022-04-07(05:26:42 +0000)

### Changes

- Enable basic lan connectivity on LLA config

## Release v1.10.2 - 2022-03-24(10:54:38 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.10.1 - 2022-03-18(07:33:31 +0000)

### Fixes

- Make LowerLayers persistent

## Release v1.10.0 - 2022-03-17(11:54:23 +0000)

### New

- [getDebugInformation] Install a getdebug information script in the prplOS

## Release v1.9.2 - 2022-03-17(10:57:22 +0000)

### Fixes

- Change NetDev subscription from Name to Alias based

## Release v1.9.1 - 2022-03-08(13:07:25 +0000)

### Fixes

- Disable lcm bridge

## Release v1.9.0 - 2022-03-03(16:45:00 +0000)

### New

- [CONFIG] add lcm interface to config

## Release v1.8.5 - 2022-02-28(10:03:28 +0000)

### Other

- [TR181-ethernet] Apply proper default values

## Release v1.8.4 - 2022-02-25(11:57:03 +0000)

### Other

- Enable core dumps by default

## Release v1.8.3 - 2022-01-31(13:32:22 +0000)

### Changes

- Integrate support for bridging in NetModel

## Release v1.8.2 - 2022-01-23(20:42:30 +0000)

### Changes

- Integrate support for bridging in NetModel

## Release v1.8.1 - 2021-11-23(17:16:21 +0000)

### Other

- [ACL] The TR-181 bridging manager must have default acl files configured

## Release v1.8.0 - 2021-11-22(09:04:14 +0000)

### New

- Use mod_netmodel to populate NetModel with the Port instances

## Release v1.7.1 - 2021-11-17(10:08:38 +0000)

### Fixes

- fix filename typo guest default odl

## Release v1.7.0 - 2021-11-04(14:44:56 +0000)

### New

- [tr181-bridging] add guest config to bridging manager

## Release v1.6.0 - 2021-10-26(13:25:34 +0000)

### New

- Add a new protected parameter containing the wifi-iface section name

## Release v1.5.3 - 2021-10-14(12:12:29 +0000)

### Fixes

- set start up order to 22

## Release v1.5.2 - 2021-10-06(07:43:39 +0000)

### Fixes

- Add mod-dmstats as a runtime dependency

## Release v1.5.1 - 2021-10-01(07:38:43 +0000)

### Other

- Create support for stats

## Release v1.5.0 - 2021-09-30(14:42:32 +0000)

### New

- Create support for vlan bridges

## Release v1.4.0 - 2021-09-07(13:07:29 +0000)

### New

- Create initial ioctl implementation

## Release v1.3.3 - 2021-09-07(11:05:45 +0000)

### Fixes

- UCI doesn't accept dashes in the names

## Release v1.3.2 - 2021-09-02(08:11:05 +0000)

### Fixes

- Disable bridges instead of removing them from the uci config

## Release v1.3.1 - 2021-08-27(09:33:38 +0000)

### Fixes

- port status will be Error is the NetDev interface does not yet exist

## Release v1.3.0 - 2021-08-24(12:16:31 +0000)

### New

- Configure bridges using UCI and netifd

## Release v1.2.0 - 2021-07-27(13:03:13 +0000)

### New

- Add Bridge and Bridge ports updates

## Release v1.1.0 - 2021-06-28(17:01:23 +0000)
### New
-[TR181 Bridging manager] Create Bridging datamodel events
  -Add functions to initialize/clean up a bridge/port info_t struct
  -Add events for creation of bridge and port instances
  -Add event for changing of bridge instances
  -Add destroy actions for bridge and port instances
  -Add events/actions to odl so the respective functions are called
  -Enable eventing during loading of the defaults/saved odl
  -Add unit tests for added functionality
  -Add sah tracing
## Release v1.0.2 - 2021-06-18(13:45:00 +0000)

### Fixes

- Update changelogs
## Release v1.0.1 - 2021-06-18(11:17:18 +0000)

### Fixes

- Change baf type to plugin
## Release v1.0.0 - 2021-06-18(09:50:26 +0000)

### New

- Initial release