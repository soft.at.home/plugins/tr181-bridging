/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include <linux/sockios.h>

#include "test_module.h"
#include "bridging_ioctl.h"
#include "../common/mock.h"

#define MOD_NAME "target_module"
#define MOD_PATH "../modules_under_test/" MOD_NAME ".so"

static amxd_dm_t dm;
static amxo_parser_t parser;

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, MOD_NAME, MOD_PATH), 0);
}

void test_has_functions(UNUSED void** state) {
    assert_true(amxm_has_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-bridge"));
    assert_true(amxm_has_function(MOD_NAME, MOD_BRIDGE_CTRL, "disable-bridge"));
    assert_true(amxm_has_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-bridge"));
    assert_true(amxm_has_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-port"));
    assert_true(amxm_has_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port"));
}

void test_add_bridge(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-bridge", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "bridge_name", "br-lan");

    ioctl_should_fail_on(SIOCBRADDBR);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-bridge", &var, &ret), -1);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-bridge", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_disable_bridge(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "disable-bridge", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "bridge_name", "br-lan");

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "disable-bridge", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_enable_bridge(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "enable-bridge", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "bridge_name", "br-lan");

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "enable-bridge", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}
void test_remove_bridge(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-bridge", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "bridge_name", "br-lan");

    ioctl_should_fail_on(SIOCBRDELBR);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-bridge", &var, &ret), 4);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-bridge", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_add_port(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &var, "netdev_index", 0);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port", &var, &ret), -1);

    amxc_var_clean(&var);
    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &var, "netdev_index", 5);
    params = amxc_var_add_key(amxc_htable_t, &var, "parameters", NULL);
    amxc_var_add_key(bool, params, "ManagementPort", true);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-port", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &var, "netdev_index", 5);
    amxc_var_add_key(cstring_t, &var, "port_name", "vlan20");
    params = amxc_var_add_key(amxc_htable_t, &var, "parameters", NULL);
    amxc_var_add_key(bool, params, "ManagementPort", false);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-port", &var, &ret), -1);

    amxc_var_add_key(cstring_t, &var, "bridge_name", "br-lan");
    amxc_var_add_key(cstring_t, params, "Name", "eth0");

    ioctl_should_fail_on(SIOCBRADDIF);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-port", &var, &ret), 2);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "add-port", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_remove_port(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port", &var, &ret), -1);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &var, "netdev_index", 0);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port", &var, &ret), -1);

    amxc_var_clean(&var);
    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &var, "netdev_index", 5);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port", &var, &ret), -1);

    amxc_var_add_key(cstring_t, &var, "bridge_name", "br-lan");
    amxc_var_add_key(cstring_t, &var, "port_name", "vlan20");
    params = amxc_var_add_key(amxc_htable_t, &var, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Name", "eth0");

    ioctl_should_fail_on(SIOCBRDELIF);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port", &var, &ret), 3);

    ioctl_should_fail_on(DO_NOT_FAIL);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_BRIDGE_CTRL, "remove-port", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_can_close_module(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
}
