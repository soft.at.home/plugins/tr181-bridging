/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__BRIDGE_MANAGER_H__)
#define __BRIDGE_MANAGER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>

#include <amxm/amxm.h>

#include <netmodel/client.h>

// Defines for port status' manually used in this code
#define PORT_STATUS_DOWN "down"
#define PORT_STATUS_ERROR "Error"

// Name of Control Parameter in the datamodel
#define BRIDGE_CTRL "BridgeController"
#define VLAN_CTRL "VLANController"
#define STP_CTRL "STPController"

// Name of the module namespace that you want to use to execute the functions
#define MOD_BRIDGE_CTRL "bridge-ctrl"
#define MOD_VLAN_CTRL "vlan-ctrl"
#define MOD_STP_CTRL "stp-ctrl"

// Define the name of the modules to be used when the controllers fail to load
#define DUMMY_MOD_NAME "mod-bridging-dummy"
#define DUMMY_VLAN_MOD_NAME "mod-vlan-dummy"

#define when_null_trace(cond, label, level, ...) if((cond) == NULL) {SAH_TRACEZ_ ## level(ME, __VA_ARGS__); goto label; }

typedef struct _bridge_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    bool mods_are_loaded;
    bool vlan_mods_are_loaded;
} bridge_app_t;

typedef struct _bridge_info {
    amxd_object_t* bridge_obj;                  // pointer to Bridging.Bridge.{i}
    char* bridge_name;                          // name of the bridge (Management port name)
    bool added;                                 // keeps track if the bridge was already added or not
    bool br_intf_find;                          // Keeps track of the presence of the interface in linux
    netmodel_query_t* query;                    // Query that monitors the netdev-bound flag on the mngmnt port interface in netmodel
} bridge_info_t;

typedef enum _port_type {
    UNKNOWN_PORT_TYPE,
    PROVIDER_NETWORK_PORT,
    CUSTOMER_NETWORK_PORT,
    CUSTOMER_EDGE_PORT,
    CUSTOMER_VLAN_PORT,
    VLAN_UNAWARE_PORT,
} port_type_t;

typedef struct _port_info {
    amxd_object_t* port_obj;                            // pointer to Bridging.Bridge.{i}.Port.{j}
    amxd_object_t* bridge_obj;                          // pointer to Bridging.Bridge.{i}
    char* netdev_intf;                                  // linux interface name
    amxb_subscription_t* netdev_status_subscription;    // subscription to be informed of port status updates
    amxb_subscription_t* netdev_new_link_subscription;  // subscription to be informed of port new port added
    uint32_t last_change;                               // time definition for time of last status change
    port_type_t port_type;                              // The type of bridge port as defined in 802.1Q
    netmodel_query_t* query;                            // netmodel query for the LowerLayers netdev name
} port_info_t;

typedef struct _vlanport_info {
    amxd_object_t* port_obj;                            // pointer to Bridging.Bridge.{i}.Port.{j}
    amxd_object_t* bridge_obj;                          // pointer to Bridging.Bridge.{i}
    amxd_object_t* vlan_obj;                            // pointer to Bridging.Bridge.{i}.VLAN.{j}
    amxd_object_t* vlanport_obj;                        // pointer to Bridging.Bridge.{i}.VLANPort.{j}
    char* vlanport_name;                                // user friendly name for the vlan
    netmodel_query_t* query;                            // netmodel query for the Port netdev name
    char* port_name;                                    // netdev name of the Port used for this VLAN
} vlanport_info_t;

typedef enum _bridge_status {
    BRIDGE_DISABLED,
    BRIDGE_ENABLED,
    BRIDGE_ERROR,
} bridge_status_t;

#define BRIDGE_STATUS_DISABLED "Disabled"
#define BRIDGE_STATUS_ENABLED "Enabled"
#define BRIDGE_STATUS_ERROR "Error"

amxd_dm_t * PRIVATE bridge_get_dm(void);
bool PRIVATE bridge_mods_are_loaded(void);
bool PRIVATE bridge_vlan_mods_are_loaded(void);

int _bridge_main(int reason,
                 amxd_dm_t* dm,
                 amxo_parser_t* parser);

#ifdef __cplusplus
}
#endif

#endif // __BRIDGE_MANAGER_H__
