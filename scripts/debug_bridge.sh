#!/bin/sh

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh

if [ ! -f "$LIBDEBUG" ]; then
    echo "No debug library found : $LIBDEBUG"
    exit 1
fi

. $LIBDEBUG


dump_bridge_full()
{
	#used for bridge-utils brctl
	show_title "show bridge($1) information"
	show_cmd brctl show $1
	show_cmd brctl showstp $1
	show_cmd brctl showmacs $1
}


#detect if we have limited busybox brctl or bridge-utils brctl. criteria is showstp function
if [ -z "$(brctl 2>&1 | grep showstp)" ] ; then
	
	#busybox brct is rather limited
	show_title "show bridges information"
	show_cmd brctl show
else
	#get bridges list from sysfs
	BRIDGES=$(grep "DEVTYPE=bridge" /sys/class/net/*/uevent |sed -E  's|^/sys/class/net/(.*)/uevent.*$|\1|')

	for BRIDGE in $BRIDGES
	do
		dump_bridge_full $BRIDGE
	done

fi 

show_title "show arp information"
show_cmd cat /proc/net/arp

