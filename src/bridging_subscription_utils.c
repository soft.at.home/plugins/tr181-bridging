/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "bridging_utils.h"
#include "bridging_dm_utils.h"
#include "bridging_subscription_utils.h"

#define ME "subscriptions"

static void _netdev_link_state_cb(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    SAH_TRACEZ_IN(ME);
    port_info_t* port = NULL;
    const char* new_state = NULL;
    const char* old_state = NULL;

    when_null_trace(priv, exit, ERROR, "private data should contain port");
    port = (port_info_t*) priv;

    old_state = GETP_CHAR(data, "parameters.State.from");
    new_state = GETP_CHAR(data, "parameters.State.to");

    (void) old_state; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "trying to change %s status from %s to %s", port->port_obj->name, old_state, new_state);
    bridging_update_port_status(port, new_state);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void netdev_link_get_initial_state(port_info_t* port, amxb_bus_ctx_t* ctx) {
    SAH_TRACEZ_IN(ME);
    int rv = AMXB_ERROR_UNKNOWN;
    amxc_var_t data;
    const char* new_state = NULL;
    amxc_string_t rel_path;

    amxc_string_init(&rel_path, 0);
    amxc_var_init(&data);

    if(ctx != NULL) {
        amxc_string_setf(&rel_path, "NetDev.Link.[%s].State", port->netdev_intf);
        rv = amxb_get(ctx, amxc_string_get(&rel_path, 0), 0, &data, 1);
        if(rv != AMXB_STATUS_OK) {
            SAH_TRACEZ_ERROR(ME, "NetDev link[%s] not found", port->netdev_intf);
        } else {
            new_state = GETP_CHAR(&data, "0.0.State");
        }
    }
    if(new_state == NULL) {
        SAH_TRACEZ_ERROR(ME, "netdev status for %s could not be found", port->netdev_intf);
        new_state = PORT_STATUS_ERROR;
    }

    bridging_update_port_status(port, new_state);
    amxc_string_clean(&rel_path);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

static void _netdev_new_link_added_cb(UNUSED const char* const sig_name,
                                      UNUSED const amxc_var_t* const data,
                                      void* const priv) {
    SAH_TRACEZ_IN(ME);
    port_info_t* port = NULL;

    when_null_trace(priv, exit, ERROR, "private data should contain port");
    port = (port_info_t*) priv;

    SAH_TRACEZ_INFO(ME, "%s interface was added to NetDev", port->netdev_intf);

    if(bridge_is_enabled(port->bridge_obj)) {
        if(port->netdev_status_subscription == NULL) {
            SAH_TRACEZ_INFO(ME, "creating a new subscription on the NetDev status");
            netdev_link_state_subscription_new(port);
        } else {
            amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev.Link.");
            when_null_trace(ctx, exit, WARNING, "ctx not known, can not get initial state");
            netdev_link_get_initial_state(port, ctx);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void netdev_link_state_subscription_new(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;

    amxc_string_init(&expr, 0);

    when_null_trace(port, exit, ERROR, "not a valid port");
    when_str_empty_trace(port->netdev_intf, exit, ERROR, "not a valid netdev interface");

    ctx = amxb_be_who_has("NetDev.Link.");
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "eobject == 'NetDev.Link.[%s].' && "
                     "contains('parameters.State')", port->netdev_intf);
    if(port->netdev_status_subscription != NULL) {
        amxb_subscription_delete(&(port->netdev_status_subscription));
    }
    amxb_subscription_new(&(port->netdev_status_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_link_state_cb, port);
    when_null_trace(port->netdev_status_subscription, exit, WARNING, "failed to create subscription");
    netdev_link_get_initial_state(port, ctx);

exit:
    amxc_string_clean(&expr);
    SAH_TRACEZ_OUT(ME);
    return;
}

void netdev_link_state_subscription_delete(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    when_null(port, exit);
    amxb_subscription_delete(&(port->netdev_status_subscription));
    bridging_update_port_status(port, PORT_STATUS_DOWN);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Create a new subscription on NetDev to listen to add events for this port
 * @param port pointer to port info structure
 */
void netdev_new_link_added_subscription(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;

    amxc_string_init(&expr, 0);

    when_null_trace(port, exit, ERROR, "not a valid port");
    when_str_empty_trace(port->netdev_intf, exit, ERROR, "not a valid netdev interface");

    ctx = amxb_be_who_has("NetDev.Link.");
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "notification == 'dm:instance-added' && path == 'NetDev.Link.' "
                     "&& parameters.Alias == '%s'",
                     port->netdev_intf);
    amxb_subscription_new(&(port->netdev_new_link_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_new_link_added_cb, port);
    when_null_trace(port->netdev_new_link_subscription, exit, WARNING, "failed to create subscription");

exit:
    amxc_string_clean(&expr);
    SAH_TRACEZ_OUT(ME);
    return;
}
