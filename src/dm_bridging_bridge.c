/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "bridging.h"
#include "dm_bridging_bridge.h"
#include "bridging_utils.h"
#include "bridging_soc_utils.h"

#define ME "br_events"

#define STD_802_1D_2004         "802.1D-2004"
#define STD_802_1Q_2005         "802.1Q-2005"
#define STD_802_1Q_2011         "802.1Q-2011"

static amxd_object_t* bridging_get_instance(amxd_object_t* const object,
                                            amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = NULL;

    when_false(amxc_var_type_of(data) == AMXC_VAR_ID_HTABLE, exit);
    when_null(GET_ARG(data, "index"), exit);
    instance = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));

exit:
    SAH_TRACEZ_OUT(ME);
    return instance;
}

static const amxc_var_t* bridging_get_max(const char* param) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = bridge_get_dm();
    amxd_object_t* bridging = amxd_dm_findf(dm, "Bridging.");
    const amxc_var_t* var = amxd_object_get_param_value(bridging, param);

    SAH_TRACEZ_OUT(ME);
    return var;
}

static const char* bridging_get_std(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    const amxc_var_t* var = amxd_object_get_param_value(instance, "Standard");
    const char* std_string = GET_CHAR(var, NULL);
    SAH_TRACEZ_OUT(ME);
    return std_string;
}

static bool bridging_is_std(const char* std, const char* value) {
    SAH_TRACEZ_IN(ME);
    bool rv = (std != NULL) && (value != NULL) && (strcmp(std, value) == 0);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;

    rv = amxd_action_param_check_is_in(object, param, reason, args, retval, priv);

    if(rv != amxd_status_ok) {
        char* input = amxc_var_dyncast(cstring_t, args);
        if((input == NULL) || (*input == 0)) {
            rv = amxd_status_ok;
        }
        free(input);
    }

    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _check_max_bridge_entries(amxd_object_t* object,
                                        amxd_param_t* param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_ok;
    const amxc_var_t* max = bridging_get_max("MaxBridgeEntries");

    rv = amxd_action_object_add_inst(object, param, reason, args, retval, (void*) max);
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to create bridge");
    }

    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _check_max_qbridge_entries(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_ok;
    amxd_object_t* instance = NULL;
    const char* std = NULL;
    amxc_llist_t entries;
    const amxc_var_t* max = bridging_get_max("MaxQBridgeEntries");

    amxc_llist_init(&entries);

    rv = amxd_action_object_add_inst(object, param, reason, args, retval, NULL);
    when_failed(rv, exit);

    instance = bridging_get_instance(object, retval);
    std = bridging_get_std(instance);

    when_false(bridging_is_std(STD_802_1Q_2005, std) ||
               bridging_is_std(STD_802_1Q_2011, std), exit);

    amxd_object_resolve_pathf(object, &entries,
                              ".[Standard == '%s' || Standard == '%s'].",
                              STD_802_1Q_2005, STD_802_1Q_2011);

    if(amxc_llist_size(&entries) > GET_UINT32(max, NULL)) {
        SAH_TRACEZ_ERROR(ME, "Bridge not created, max number of Q bridges exceeded");
        rv = amxd_status_invalid_value;
    }

exit:
    amxc_llist_clean(&entries, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _check_max_dbridge_entries(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_ok;
    amxd_object_t* instance = NULL;
    const char* std = NULL;
    amxc_llist_t entries;
    const amxc_var_t* max = bridging_get_max("MaxDBridgeEntries");

    amxc_llist_init(&entries);

    rv = amxd_action_object_add_inst(object, param, reason, args, retval, NULL);
    when_failed(rv, exit);

    instance = bridging_get_instance(object, retval);
    std = bridging_get_std(instance);

    when_false(bridging_is_std(STD_802_1D_2004, std), exit);

    amxd_object_resolve_pathf(object, &entries,
                              ".[Standard == '%s'].",
                              STD_802_1D_2004);

    if(amxc_llist_size(&entries) > GET_UINT32(max, NULL)) {
        SAH_TRACEZ_ERROR(ME, "Bridge not created, max number of D bridges exceeded");
        rv = amxd_status_invalid_value;
    }

exit:
    amxc_llist_clean(&entries, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _check_max_std_entries(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_ok;
    amxc_llist_t entries;
    const amxc_var_t* max = NULL;
    const char* name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);

    amxc_llist_init(&entries);

    when_true(amxd_object_get_type(object) == amxd_object_template, exit);

    rv = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(rv, exit);

    if(bridging_is_std(GET_CHAR(args, NULL), STD_802_1D_2004)) {
        max = bridging_get_max("MaxDBridgeEntries");
        amxd_object_resolve_pathf(amxd_object_get_parent(object), &entries,
                                  ".[Standard == '%s' && Alias != '%s'].",
                                  STD_802_1D_2004, name);
    } else {
        max = bridging_get_max("MaxQBridgeEntries");
        amxd_object_resolve_pathf(amxd_object_get_parent(object), &entries,
                                  ".[(Standard == '%s' || Standard == '%s') && Alias != '%s'].",
                                  STD_802_1Q_2005, STD_802_1Q_2011, name);
    }

    if(amxc_llist_size(&entries) >= GET_UINT32(max, NULL)) {
        SAH_TRACEZ_ERROR(ME, "Bridge not created, max number of entries for this type exceeded");
        rv = amxd_status_invalid_value;
    }

exit:
    amxc_llist_clean(&entries, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _bridging_bridge_added(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    bridge_info_t* bridge = NULL;
    amxd_object_t* bridge_templ_obj = NULL;
    amxd_object_t* bridge_obj = NULL;
    amxd_dm_t* dm = bridge_get_dm();
    amxd_status_t rv = amxd_status_unknown_error;

    bridge_templ_obj = amxd_dm_signal_get_object(dm, data);
    when_null_trace(bridge_templ_obj, exit, ERROR, "could not get bridge template object");

    bridge_obj = amxd_object_get_instance(bridge_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(bridge_obj, exit, ERROR,
                    "could not get bridge with index %d", GET_UINT32(data, "index"));

    when_not_null(bridge_obj->priv, exit);

    rv = bridge_info_new(&bridge);
    when_failed(rv, exit);

    bridge->bridge_obj = bridge_obj;
    // Save bridge in the bridge instance priv so we can use it later
    bridge->bridge_obj->priv = bridge;

    SAH_TRACEZ_INFO(ME, "new bridge %s added",
                    amxd_object_get_name(bridge_obj, AMXD_OBJECT_NAMED));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _bridging_bridge_removed(amxd_object_t* object,
                                       UNUSED amxd_param_t* param,
                                       amxd_action_t reason,
                                       UNUSED const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    bridge_info_t* bridge = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;

    SAH_TRACEZ_WARNING(ME, "removing bridge %s",
                       amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");

    // The remove function is also called for the Bridge template object, we only want to destroy
    // the instances
    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "skipping bridge template, only remove instances of bridge");
        rv = amxd_status_ok;
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "removing bridge %s from config", amxd_object_get_name(object, AMXD_OBJECT_NAMED));
    mod_bridge_remove(object);

    when_null_trace(object->priv, exit, WARNING, "object has no private data, can't destroy bridge");

    bridge = (bridge_info_t*) object->priv;
    rv = bridge_info_clean(&bridge);
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _bridging_bridge_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* bridge_obj = NULL;
    amxd_dm_t* dm = bridge_get_dm();

    SAH_TRACEZ_INFO(ME, "changing a bridge");
    bridge_obj = amxd_dm_signal_get_object(dm, data);
    when_null_trace(bridge_obj, exit, ERROR, "calling object could not be found");

    bridge_check_and_apply_config(bridge_obj);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
