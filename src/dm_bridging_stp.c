/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "dm_bridging_stp.h"
#include "bridging_utils.h"
#include "bridging_soc_utils.h"

#define ME "stp_events"

void _bridging_stp_changed(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* stp_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    amxd_object_t* bridge_obj = amxd_object_findf(stp_obj, "^.");
    bridge_info_t* bridge_info = NULL;
    amxc_var_t args;

    amxc_var_init(&args);

    when_null_trace(bridge_obj, exit, ERROR, "Bridge object is NULL");
    bridge_info = (bridge_info_t*) bridge_obj->priv;
    when_null_trace(bridge_info, exit, ERROR, "Bridge object private data is NULL");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_for_each(var, GET_ARG(data, "parameters")) {
        amxc_var_set_key(&args, amxc_var_key(var), GET_ARG(var, "to"), AMXC_VAR_FLAG_COPY);
    }
    amxc_var_add_key(cstring_t, &args, "bridge_name", bridge_info->bridge_name);

    mod_configure_stp(bridge_obj, &args);

exit:
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);
}

void _bridging_agingtime_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* bridge_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    bridge_info_t* bridge_info = (bridge_info_t*) bridge_obj->priv;
    amxc_var_t args;

    amxc_var_init(&args);

    when_null_trace(bridge_info, exit, ERROR, "Bridge info private data is NULL");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "bridge_name", bridge_info->bridge_name);
    amxc_var_add_key(uint64_t, &args, "AgingTime", amxc_var_dyncast(uint64_t, GETP_ARG(data, "parameters.AgingTime.to")));

    mod_configure_stp(bridge_obj, &args);
exit:
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t _is_valid_bridge_prio(UNUSED amxd_object_t* const object,
                                    UNUSED amxd_param_t* const param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {

    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    uint32_t bridge_prio = GET_UINT32(args, NULL);

    if(reason != action_param_validate) {
        rv = amxd_status_invalid_action;
        goto exit;
    }

    if(bridge_prio % 4096 != 0) {
        rv = amxd_status_invalid_value;
        goto exit;
    }

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _read_stp_status(amxd_object_t* const object,
                               UNUSED amxd_param_t* const param,
                               amxd_action_t reason,
                               UNUSED const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               UNUSED void* priv) {

    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_object_t* bridge_obj = amxd_object_findf(object, "^.");
    bridge_info_t* bridge_info = NULL;
    amxc_var_t params;
    amxc_var_t ret;
    const char* status = NULL;

    amxc_var_init(&params);
    amxc_var_init(&ret);

    if(reason != action_param_read) {
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(bridge_obj, exit, ERROR, "Bridge object is NULL");
    bridge_info = (bridge_info_t*) bridge_obj->priv;
    when_null_trace(bridge_info, exit, ERROR, "Bridge object private data is NULL");

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "bridge_name", bridge_info->bridge_name);

    when_failed_status(mod_bridging_execute_stp_function("is-stp-enabled", bridge_obj, &params, &ret), exit, status = "Error");

    if(amxd_object_get_value(bool, object, "Enable", NULL)) {
        if(GET_BOOL(&ret, "Enabled")) {
            status = "Enabled";
        } else {
            status = "Error";
        }
    } else {
        if(!GET_BOOL(&ret, "Enabled")) {
            status = "Disabled";
        } else {
            status = "Error";
        }
    }
    amxc_var_set(cstring_t, retval, status);
    rv = amxd_status_ok;
exit:
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
