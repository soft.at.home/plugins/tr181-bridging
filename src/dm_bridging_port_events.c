/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "dm_bridging_port.h"
#include "bridging_utils.h"
#include "bridging_soc_utils.h"
#include "bridging_subscription_utils.h"

#define ME "port_events"

static void set_lower_layer_to(amxd_object_t* management_port_obj, const char* lower_layer) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_arg;
    amxd_dm_t* dm = bridge_get_dm();
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null_trace(management_port_obj, exit, WARNING, "port object can not be NULL, LowerLayers not set");
    SAH_TRACEZ_INFO(ME, "setting LowerLayers to '%s'", lower_layer);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, management_port_obj);
    amxd_trans_set_value(csv_string_t, &trans, "LowerLayers", lower_layer);
    rv = amxd_trans_apply(&trans, dm);

    if(rv != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Failed to set LowerLayers, returned %d", rv);
    }

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return;
}

void set_lower_layer(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* management_port = NULL;
    amxc_llist_t paths;
    amxc_string_t lower_layer_string;
    bridge_info_t* bridge = NULL;
    const char* lower_layer = "";

    amxc_string_init(&lower_layer_string, 0);
    amxc_llist_init(&paths);

    SAH_TRACEZ_INFO(ME, "setting LowerLayers");
    when_null_trace(bridge_obj, exit, WARNING, "bridge object can not be NULL, LowerLayers not set");
    management_port = amxd_object_findf(bridge_obj, ".Port.[ManagementPort == true]");
    when_null_trace(management_port, exit, WARNING, "no management port could be found");

    bridge = (bridge_info_t*) bridge_obj->priv;
    when_null_trace(bridge, exit, ERROR, "no bridge found");
    // make sure to clean before re-assigning the name
    free(bridge->bridge_name);
    bridge->bridge_name = amxd_object_get_value(cstring_t, management_port, "Name", NULL);

    if(!(bridge->br_intf_find)) {
        // If the bridge interface is not found in linux, clear the lower layers
        set_lower_layer_to(management_port, "");
        goto exit;
    }

    amxd_object_resolve_pathf(bridge_obj, &paths, ".Port.[ManagementPort == false]");

    if(!amxc_llist_is_empty(&paths)) {
        amxc_string_join_llist(&lower_layer_string, &paths, ',');
        // Make sure that paths are prepended with "Device."
        amxc_string_replace(&lower_layer_string, "Bridging", "Device.Bridging", UINT32_MAX);
        lower_layer = amxc_string_get(&lower_layer_string, 0);
    }

    set_lower_layer_to(management_port, lower_layer);

exit:
    amxc_llist_clean(&paths, amxc_string_list_it_free);
    amxc_string_clean(&lower_layer_string);
    SAH_TRACEZ_OUT(ME);
    return;
}

static int _port_vlanport_remove(UNUSED amxd_object_t* templ,
                                 amxd_object_t* instance,
                                 UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    vlanport_info_t* vlanport = NULL;
    bool enabled = amxd_object_get_bool(instance, "Enable", NULL);
    when_null(instance, exit);
    when_null(instance->priv, exit);
    vlanport = (vlanport_info_t*) instance->priv;

    // If the vlanport is enabled, the current config should be removed first
    if(enabled) {
        mod_bridge_vlanport_remove(vlanport);
    }
    vlanport->port_obj = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int _port_vlanport_disable(UNUSED amxd_object_t* templ,
                                  amxd_object_t* instance,
                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    when_null(instance, exit);

    rv = amxd_object_set_value(bool, instance, "Enable", false);
    rv |= amxd_object_set_value(cstring_t, instance, "Port", "");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int _port_vlanport_priority_reset(UNUSED amxd_object_t* templ,
                                         amxd_object_t* instance,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    vlanport_info_t* vlanport = NULL;
    bool enabled = amxd_object_get_bool(instance, "Enable", NULL);

    when_null(instance, exit);
    when_null(instance->priv, exit);
    vlanport = (vlanport_info_t*) instance->priv;

    // If the vlanport is enabled, the current config should recreated
    if(enabled) {
        mod_vlan_execute_function("set-vlan-priority", vlanport);
    }

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _bridging_port_added(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = bridge_get_dm();
    port_info_t* port = NULL;
    amxd_object_t* port_templ_obj = NULL;
    amxd_object_t* bridge_obj = NULL;
    amxd_object_t* port_obj = NULL;
    const char* intf_name = NULL;
    bool is_mgmt_port = false;
    amxd_param_t* lowerlayers = NULL;

    port_templ_obj = amxd_dm_signal_get_object(dm, data);
    when_null_trace(port_templ_obj, exit, ERROR, "could not get port template object");
    bridge_obj = amxd_object_get_parent(port_templ_obj);
    when_null_trace(bridge_obj, exit, ERROR, "could not get bridge object");

    port_obj = amxd_object_get_instance(port_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(port_obj, exit, ERROR, "could not get port with index %d", GET_UINT32(data, "index"));

    when_not_null(port_obj->priv, exit);

    SAH_TRACEZ_INFO(ME, "a port was added to the datamodel");
    intf_name = GETP_CHAR(data, "parameters.Name");
    is_mgmt_port = GETP_BOOL(data, "parameters.ManagementPort");

    rv = port_info_new(&port, bridge_obj, is_mgmt_port ? intf_name : NULL);
    when_failed(rv, exit);

    port->port_obj = port_obj;
    port->port_obj->priv = port;
    port_type(port, GETP_CHAR(data, "parameters.Type"));

    set_lower_layer(bridge_obj);

    lowerlayers = amxd_object_get_param_def(port_obj, "LowerLayers");

    if(GETP_BOOL(data, "parameters.Enable")) {
        if(is_mgmt_port) {
            netdev_new_link_added_subscription(port);
            mngmnt_port_create_flag_query(port);
            bridge_check_and_apply_config(bridge_obj);
            rv = amxd_param_set_attr(lowerlayers, amxd_pattr_persistent, false);
            when_failed_trace(rv, exit, ERROR, "Failed to remove the persistency flag");
        } else {
            mod_bridge_port_add(port);
        }
    }

    rv = amxd_param_set_attr(lowerlayers, amxd_pattr_read_only, is_mgmt_port);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the read only flag", is_mgmt_port ? "add" : "remove");

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _bridging_port_removed(amxd_object_t* object,
                                     UNUSED amxd_param_t* param,
                                     amxd_action_t reason,
                                     UNUSED const amxc_var_t* const args,
                                     UNUSED amxc_var_t* const retval,
                                     UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    port_info_t* port = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxc_string_t spath;
    bool enabled = amxd_object_get_value(bool, object, "Enable", NULL);
    char* port_path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);

    amxc_string_init(&spath, 0);

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");
    when_str_empty(port_path, exit);

    SAH_TRACEZ_INFO(ME, "Removing port: %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    // The remove function is also called for the Port template object, we only want to destroy
    // the instances
    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "Skipping port template, only remove instances of port");
        rv = amxd_status_ok;
        goto exit;
    }

    when_null_trace(object->priv, exit, WARNING, "Object has no private data, can't destroy port");
    port = (port_info_t*) object->priv;

    amxc_string_setf(&spath, ".^.^.VLANPort.[Port == 'Device.%s']", port_path);
    amxd_object_for_all(object, amxc_string_get(&spath, 0), _port_vlanport_remove, NULL);

    if(enabled) {
        // no need to use remove_port, the whole port will be cleaned up
        SAH_TRACEZ_INFO(ME, "Remove port %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));
        mod_bridging_execute_port_function("remove-port", port);
    }

    rv = port_info_clean(&port);
exit:
    free(port_path);
    amxc_string_clean(&spath);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _bridging_managementport_changed(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = bridge_get_dm();
    port_info_t* port = NULL;
    amxd_object_t* port_obj = NULL;
    bool management_port;
    amxd_param_t* lowerlayers = NULL;

    management_port = GETP_BOOL(data, "parameters.ManagementPort.to");

    port_obj = amxd_dm_signal_get_object(dm, data);
    when_null_trace(port_obj, exit, ERROR, "could not get port object");
    when_null_trace(port_obj->priv, exit, ERROR, "object has no private data");

    port = (port_info_t*) port_obj->priv;
    when_null_trace(port->bridge_obj, exit, ERROR, "bridge object is NULL");

    if(management_port) {
        set_lower_layer(port->bridge_obj);
    } else {
        set_lower_layer_to(port_obj, "");
    }

    bridge_check_and_apply_config(port->bridge_obj);

    lowerlayers = amxd_object_get_param_def(port_obj, "LowerLayers");
    amxd_param_set_attr(lowerlayers, amxd_pattr_read_only, management_port);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_port_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = bridge_get_dm();
    port_info_t* port = NULL;
    amxd_object_t* port_obj = NULL;

    port_obj = amxd_dm_signal_get_object(dm, data);
    when_null_trace(port_obj, exit, ERROR, "could not get port object");
    when_null_trace(port_obj->priv, exit, ERROR, "object has no private data");

    port = (port_info_t*) port_obj->priv;
    when_null_trace(port->bridge_obj, exit, ERROR, "bridge object is NULL");

    bridge_check_and_apply_config(port->bridge_obj);

    when_true(amxd_object_get_value(bool, port_obj, "ManagementPort", NULL), exit);
    if(GETP_BOOL(data, "parameters.Enable.to")) {
        mod_bridge_port_add(port);
    } else {
        mod_bridge_port_remove(port);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_port_type_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* port_obj = NULL;
    port_info_t* port = NULL;

    port_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    when_null_trace(port_obj, exit, ERROR, "could not get port object");
    when_null_trace(port_obj->priv, exit, ERROR, "object has no private data");

    port = (port_info_t*) port_obj->priv;
    when_null_trace(port->bridge_obj, exit, ERROR, "bridge object is NULL");

    when_true(amxd_object_get_value(bool, port_obj, "ManagementPort", NULL), exit);
    port_type(port, GETP_CHAR(data, "parameters.Type.to"));
    if((port->port_type == VLAN_UNAWARE_PORT) &&
       amxd_object_get_value(bool, port_obj, "Enable", NULL)) {
        mod_bridge_port_add(port);
    } else {
        mod_bridge_port_remove(port);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_port_default_priority_changed(UNUSED const char* const sig_name,
                                             const amxc_var_t* const data,
                                             UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t spath;
    char* trimmed_path = trim_final_dot(GET_CHAR(data, "path"));
    amxd_object_t* port_templ_obj = amxd_object_get_parent(amxd_dm_signal_get_object(bridge_get_dm(), data));

    amxc_string_init(&spath, 0);

    amxc_string_setf(&spath, ".^.VLANPort.[Port == 'Device.%s' && VlanPriority == -1]", trimmed_path);
    amxd_object_for_all(port_templ_obj, amxc_string_get(&spath, 0), _port_vlanport_priority_reset, NULL);

    free(trimmed_path);
    amxc_string_clean(&spath);
    SAH_TRACEZ_OUT(ME);
}

void _bridging_port_lower_layers_changed(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* port_obj = NULL;
    port_info_t* port = NULL;

    port_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    when_null_trace(port_obj, exit, ERROR, "could not get port object");
    when_null_trace(port_obj->priv, exit, ERROR, "object has no private data");

    when_true(amxd_object_get_value(bool, port_obj, "ManagementPort", NULL), exit);

    port = (port_info_t*) port_obj->priv;
    mod_bridge_port_remove(port);
    if(amxd_object_get_value(bool, port_obj, "Enable", NULL)) {
        mod_bridge_port_add(port);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_port_not_implemented(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    (void) data; // use variable when tracing is disabled
    SAH_TRACEZ_ERROR(ME, "%s has no functionality implemented",
                     amxc_var_key(GETP_ARG(data, "parameters.0")));
    SAH_TRACEZ_OUT(ME);
}

void _bridging_port_deleted(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t spath;
    amxd_object_t* port_templ_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    amxd_object_t* bridge_obj = amxd_object_get_parent(port_templ_obj);

    amxc_string_init(&spath, 0);

    SAH_TRACEZ_INFO(ME, "Port deleted from the datamodel");
    when_null_trace(port_templ_obj, exit, ERROR, "could not get port template object");
    when_null_trace(bridge_obj, exit, ERROR, "could not get bridge object");

    set_lower_layer(bridge_obj);

    bridge_check_and_apply_config(bridge_obj);

    amxc_string_setf(&spath, ".^.VLANPort.[Port == 'Device.%s%d']", GETP_CHAR(data, "path"), GETP_INT32(data, "index"));
    amxd_object_for_all(port_templ_obj, amxc_string_get(&spath, 0), _port_vlanport_disable, NULL);

    amxc_string_clean(&spath);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
