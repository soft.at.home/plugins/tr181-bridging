/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "bridging_utils.h"
#include "bridging_dm_utils.h"
#include "bridging_soc_utils.h"
#include "bridging_subscription_utils.h"

#define ME "bridging"

static int is_dot(int c) {
    return (c == '.') ? 1 : 0;
}

/**
 * @brief Function to trim the trailing dot from a char string if there is one
 * @param path char string containing the path that should have the dot removed
 * @return returns pointer to a string buffer containing the path string without a trailing dot
 * @note This function allocates memory to store the trimmed path. The memory needs to be freed using "free" if not needed anymore
 */
char* trim_final_dot(const char* path) {
    amxc_string_t trimmed_path;
    amxc_string_init(&trimmed_path, 0);

    when_str_empty_trace(path, exit, ERROR, "No path given to trim");

    amxc_string_set(&trimmed_path, path);
    amxc_string_trimr(&trimmed_path, is_dot);
exit:
    return amxc_string_take_buffer(&trimmed_path);
}
static int _port_add_cb(UNUSED amxd_object_t* templ,
                        amxd_object_t* instance,
                        UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    port_info_t* port = NULL;

    when_null_trace(instance, exit, ERROR, "could not get port object");
    when_null_trace(instance->priv, exit, ERROR, "object %s has no private data", instance->name);

    port = (port_info_t*) instance->priv;
    mod_bridge_port_add(port);
    netdev_link_state_subscription_new(port);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int _port_remove_cb(UNUSED amxd_object_t* templ,
                           amxd_object_t* instance,
                           UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    port_info_t* port = NULL;

    when_null_trace(instance, exit, ERROR, "could not get port object");
    when_null_trace(instance->priv, exit, ERROR, "object %s has no private data", instance->name);

    port = (port_info_t*) instance->priv;
    mod_bridge_port_remove(port);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int _vlanport_add_cb(UNUSED amxd_object_t* templ,
                            amxd_object_t* instance,
                            UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = NULL;

    when_null_trace(instance, exit, ERROR, "could not get vlanport object");
    when_null_trace(instance->priv, exit, INFO, "object %s has no private data", instance->name);

    vlanport = (vlanport_info_t*) instance->priv;
    mod_bridge_vlanport_add(vlanport);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int _vlanport_remove_cb(UNUSED amxd_object_t* templ,
                               amxd_object_t* instance,
                               UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = NULL;

    when_null_trace(instance, exit, ERROR, "could not get vlanport object");
    when_null_trace(instance->priv, exit, INFO, "object %s has no private data", instance->name);

    vlanport = (vlanport_info_t*) instance->priv;
    mod_bridge_vlanport_remove(vlanport);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static bool bridge_has_valid_mp(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    bool mp_is_valid = false;
    amxd_object_t* management_port = NULL;
    management_port = amxd_object_findf(bridge_obj, ".Port.[ManagementPort == true]");
    when_null_trace(management_port, exit, WARNING, "no management port could be found");

    mp_is_valid = amxd_object_get_value(bool, management_port, "Enable", NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return mp_is_valid;
}

static int get_vlanport_info_parameters(vlanport_info_t* vlanport, amxc_var_t* bridge_params) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* port_params = NULL;
    amxc_var_t* vlan_params = NULL;
    amxc_var_t* vlanport_params = NULL;

    amxc_var_set_type(bridge_params, AMXC_VAR_ID_HTABLE);
    when_null_trace(vlanport, exit, ERROR, "vlanport can not be NULL");
    when_null_trace(vlanport->bridge_obj, exit, ERROR, "unknown bridge");
    when_null_trace(vlanport->port_obj, exit, ERROR, "no port configured in vlanport");
    when_null_trace(vlanport->vlan_obj, exit, ERROR, "no vlan configured in vlanport");

    amxd_object_get_params(vlanport->bridge_obj, bridge_params, amxd_dm_access_protected);
    port_params = amxc_var_add_key(amxc_htable_t, bridge_params, "port", NULL);
    vlan_params = amxc_var_add_key(amxc_htable_t, bridge_params, "vlan", NULL);
    vlanport_params = amxc_var_add_key(amxc_htable_t, bridge_params, "vlanport", NULL);
    amxd_object_get_params(vlanport->port_obj, port_params, amxd_dm_access_protected);
    amxd_object_get_params(vlanport->vlan_obj, vlan_params, amxd_dm_access_protected);
    amxd_object_get_params(vlanport->vlanport_obj, vlanport_params, amxd_dm_access_protected);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool validate_tagged_parameters(amxc_var_t* bridge_params) {
    SAH_TRACEZ_IN(ME);
    bool config_is_valid = false;
    const char* port_type = NULL;
    const char* port_frame_types = NULL;

    port_type = GETP_CHAR(bridge_params, "port.Type");
    when_str_empty_trace(port_type, exit, ERROR,
                         "invalid type for vlan port, can not be NULL or empty");
    when_false_trace(strcmp(port_type, "CustomerVLANPort") == 0, exit, ERROR,
                     "invalid type for vlan port, '%s' is not a valid type for an untagged config", port_type);

    port_frame_types = GETP_CHAR(bridge_params, "port.AcceptableFrameTypes");
    when_str_empty_trace(port_frame_types, exit, ERROR,
                         "invalid port type, can not be NULL or empty");
    when_false_trace(strcmp(port_frame_types, "AdmitOnlyVLANTagged") == 0, exit, ERROR,
                     "'%s' is not a valid frame type for an untagged config", port_frame_types);

    config_is_valid = true;

exit:
    SAH_TRACEZ_OUT(ME);
    return config_is_valid;
}

static int create_nm_query(netmodel_query_t** query, const char* intf, netmodel_callback_t fn, void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    if(*query != NULL) {
        netmodel_closeQuery(*query);
        *query = NULL;
    }

    *query = netmodel_openQuery_getFirstParameter(intf,
                                                  "bridging-manager",
                                                  "NetDevName",
                                                  "netdev-bound && !inbridge",
                                                  netmodel_traverse_down,
                                                  fn,
                                                  priv);

    when_null_trace(*query, exit, ERROR,
                    "Failed to create netmodel query for NetDevName on intf %s", intf);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void close_nm_query(netmodel_query_t** query, char** intf_name) {
    SAH_TRACEZ_IN(ME);
    // if query is NULL, intf_name does not come from the query result (e.g. management ports have a statically defined name)
    when_null(*query, exit);

    netmodel_closeQuery(*query);
    *query = NULL;
    free(*intf_name);
    *intf_name = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void close_nm_flag_query(netmodel_query_t** query) {
    SAH_TRACEZ_IN(ME);
    netmodel_closeQuery(*query);
    *query = NULL;
    SAH_TRACEZ_OUT(ME);
}

static void vlan_port_netdevname_updated(UNUSED const char* sig,
                                         const amxc_var_t* data,
                                         void* priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = (vlanport_info_t*) priv;
    const char* new_name = NULL;
    when_null_trace(vlanport, exit, ERROR, "Private data is NULL");

    new_name = amxc_var_constcast(cstring_t, data);
    when_null_trace(new_name, exit, WARNING, "Received NULL instead of a string");
    SAH_TRACEZ_INFO(ME, "VLAN Port netdev name changing from %s to %s",
                    vlanport->port_name != NULL ? vlanport->port_name : "", new_name);

    if(!str_empty(vlanport->port_name)) {
        // We have a port name
        if(str_empty(new_name) || (strcmp(vlanport->port_name, new_name) != 0)) {
            // The new name is empty or different than the current one -> remove current vlan
            mod_vlan_execute_function("destroy-vlan", vlanport);
            free(vlanport->port_name);
            vlanport->port_name = NULL;
        } else {
            // The new name is identical to the current one -> do nothing
            goto exit;
        }
    }

    bridging_update_port_name(vlanport->port_obj, new_name);
    // The current name will always be empty at this point (was empty or we just cleared it)
    // If the new name is not empty -> set it and create the vlan
    if(!str_empty(new_name)) {
        vlanport->port_name = strdup(new_name);
        mod_vlan_execute_function("create-vlan", vlanport);
        mod_bridging_execute_vlanport_function("add-port", vlanport);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int vlan_create_nm_query(vlanport_info_t* vlanport) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* port = amxd_object_get_value(cstring_t, vlanport->vlanport_obj, "Port", NULL);
    when_null_trace(port, exit, ERROR, "Failed to get Port parameter for %s", vlanport->port_name);

    rv = create_nm_query(&(vlanport->query), port, vlan_port_netdevname_updated, vlanport);

exit:
    free(port);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void vlan_close_nm_query(vlanport_info_t* vlanport) {
    SAH_TRACEZ_IN(ME);
    close_nm_query(&(vlanport->query), &(vlanport->port_name));
    SAH_TRACEZ_OUT(ME);
}

static void port_netdevname_updated(UNUSED const char* sig,
                                    const amxc_var_t* data,
                                    void* priv) {
    SAH_TRACEZ_IN(ME);
    port_info_t* port = (port_info_t*) priv;
    const char* new_name = NULL;
    when_null_trace(port, exit, ERROR, "Private data is NULL");

    new_name = amxc_var_constcast(cstring_t, data);
    when_null_trace(new_name, exit, WARNING, "Received NULL instead of a string");
    SAH_TRACEZ_INFO(ME, "Port interface netdev name changing from %s to %s",
                    port->netdev_intf != NULL ? port->netdev_intf : "", new_name);

    if(!str_empty(port->netdev_intf)) {
        // We have an intf name
        if(str_empty(new_name) || (strcmp(port->netdev_intf, new_name) != 0)) {
            // The new name is empty or different than the current one -> remove port
            mod_bridging_execute_port_function("remove-port", port);
            netdev_link_state_subscription_delete(port);
            free(port->netdev_intf);
            port->netdev_intf = NULL;
        } else {
            // The new name is identical to the current one -> do nothing
            goto exit;
        }
    }

    bridging_update_port_name(port->port_obj, new_name);
    // The current name will always be empty at this point (was empty or we just cleared it)
    // If the new name is not empty -> set it and add the port
    if(!str_empty(new_name)) {
        port->netdev_intf = strdup(new_name);
        if(mod_bridging_execute_port_function("add-port", port) != 0) {
            bridging_update_port_status(port, PORT_STATUS_ERROR);
            goto exit;
        }
        if(bridge_is_enabled(port->bridge_obj)) {
            netdev_link_state_subscription_new(port);
        }
    } else {
        bridging_update_port_status(port, PORT_STATUS_ERROR);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int port_create_nm_query(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* intf = amxd_object_get_value(cstring_t, port->port_obj, "LowerLayers", NULL);
    when_null_trace(intf, exit, ERROR, "Failed to get LowerLayers parameter for %s", port->port_obj->name);

    rv = create_nm_query(&(port->query), intf, port_netdevname_updated, port);

exit:
    free(intf);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void port_close_nm_query(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    close_nm_query(&(port->query), &(port->netdev_intf));
    SAH_TRACEZ_OUT(ME);
}

static void bridge_close_flag_query(bridge_info_t* bridge) {
    SAH_TRACEZ_IN(ME);
    close_nm_flag_query(&(bridge->query));
    SAH_TRACEZ_OUT(ME);
}

static void toggle_bridge_intf_find(const char* sig,
                                    const amxc_var_t* data,
                                    void* priv) {
    SAH_TRACEZ_IN(ME);
    bridge_info_t* bridge = (bridge_info_t*) priv;
    when_null_trace(bridge, exit, ERROR, "Private data is NULL");

    // Check if query is opened correctly
    if(amxc_var_is_null(data)) {
        if((sig != NULL) && (strcmp(sig, "Resolver") == 0)) {
            SAH_TRACEZ_INFO(ME, "Query interface path is still being resolved");
        } else {
            SAH_TRACEZ_ERROR(ME, "Query is closed in NetModel, closing local query");
            netmodel_closeQuery(bridge->query);
            bridge->query = NULL;
        }
        goto exit;
    }

    bridge->br_intf_find = GET_BOOL(data, NULL);
    set_lower_layer(bridge->bridge_obj);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int create_nm_hasflag_query(netmodel_query_t** query, const char* intf, netmodel_callback_t fn, void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    if(*query != NULL) {
        netmodel_closeQuery(*query);
        *query = NULL;
    }

    *query = netmodel_openQuery_hasFlag(intf,
                                        "bridging-manager",
                                        "netdev-bound",
                                        NULL,
                                        netmodel_traverse_this,
                                        fn,
                                        priv);

    when_null_trace(*query, exit, ERROR,
                    "Failed to create netmodel query for the netdev-bound flag on intf %s", intf);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mngmnt_port_create_flag_query(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    bridge_info_t* bridge = NULL;
    char* intf = amxd_object_get_path(port->port_obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_null_trace(intf, exit, ERROR, "Failed to get the intf path for %s", port->port_obj->name);

    bridge = (bridge_info_t*) port->bridge_obj->priv;
    rv = create_nm_hasflag_query(&(bridge->query), intf, toggle_bridge_intf_find, bridge);
exit:
    free(intf);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool vlanport_config_valid(vlanport_info_t* vlanport) {
    SAH_TRACEZ_IN(ME);
    bool config_is_valid = false;
    amxc_var_t bridge_params;
    const char* bridge_standard = NULL;

    amxc_var_init(&bridge_params);

    when_failed(get_vlanport_info_parameters(vlanport, &bridge_params), exit);

    when_false_trace(GET_BOOL(&bridge_params, "Enable"), exit, ERROR,
                     "bridge is not enable");

    bridge_standard = GET_CHAR(&bridge_params, "Standard");
    when_str_empty_trace(bridge_standard, exit, ERROR,
                         "invalid bridge standard, can not be NULL or empty");
    when_false_trace(strcmp(bridge_standard, "802.1D-2004") != 0, exit, ERROR,
                     "invalid bridge standard, '%s' does not support vlans", bridge_standard);

    when_false_trace(GETP_BOOL(&bridge_params, "port.Enable"), exit, ERROR,
                     "the port in this vlanport is not enable");
    when_false_trace(GETP_BOOL(&bridge_params, "vlan.Enable"), exit, ERROR,
                     "the vlan in this vlanport is not enable");

    if(!GETP_BOOL(&bridge_params, "vlanport.Untagged")) {
        config_is_valid = validate_tagged_parameters(&bridge_params);
    }

exit:
    amxc_var_clean(&bridge_params);
    SAH_TRACEZ_OUT(ME);
    return config_is_valid;
}

/**
 * @brief Checks if the bridge status is enabled
 * @param bridge_obj the bridge object pointer
 * @return true if the bridge status is enabled, false otherwise
 */
bool bridge_is_enabled(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    bool bridge_enabled = false;
    cstring_t current_status = NULL;

    when_null_trace(bridge_obj, exit, ERROR, "no port object given");
    current_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    when_str_empty(current_status, exit);
    bridge_enabled = (strcmp(current_status, BRIDGE_STATUS_ENABLED) == 0);

exit:
    free(current_status);
    SAH_TRACEZ_OUT(ME);
    return bridge_enabled;
}

/**
 * @brief converst a path to an object pointer
 * @param device_path a char* path starting with Device. pointing to an object
 * @return pointer to the object referenced by the path
 */
amxd_object_t* get_object_from_device_path(const char* device_path) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    amxd_object_t* object = NULL;
    int start_pos = -1;

    amxc_string_init(&path, 0);
    when_str_empty(device_path, exit);

    amxc_string_set(&path, device_path);
    start_pos = amxc_string_search(&path, ".", 0);

    when_false_trace(start_pos > 0, exit, ERROR, "failed to trim path, not a valid path");
    object = amxd_dm_findf(bridge_get_dm(), "%s", amxc_string_get(&path, start_pos + 1));
exit:
    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return object;
}

/**
 * @brief Call the bridging implemenation module to add a port to the bridge
 * @param port the port info structure that should be added
 */
void mod_bridge_port_add(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    char* ll = NULL;
    when_null(port, exit);
    when_false(port->port_type == VLAN_UNAWARE_PORT, exit);
    when_true(amxd_object_get_value(bool, port->port_obj, "ManagementPort", NULL), exit);
    when_false(amxd_object_get_value(bool, port->bridge_obj, "Enable", NULL), exit);

    ll = amxd_object_get_value(cstring_t, port->port_obj, "LowerLayers", NULL);
    if(str_empty(ll)) {
        SAH_TRACEZ_WARNING(ME, "LowerLayers is empty, not adding port '%s'", port->port_obj->name);
        bridging_update_port_status(port, PORT_STATUS_ERROR);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "adding port '%s' to the config", port->port_obj->name);
    port_create_nm_query(port);

exit:
    free(ll);
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Call the bridging implemenation module to remove a port from the bridge
 * @param port the port info structure that should be removed
 */
void mod_bridge_port_remove(port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    when_null(port, exit);
    SAH_TRACEZ_INFO(ME, "removing port %s from the config", port->port_obj->name);

    mod_bridging_execute_port_function("remove-port", port);
    port_close_nm_query(port);
    netdev_link_state_subscription_delete(port);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Creates the new vlan interface and adds it to the bridge
 * @param vlanport The vlanport info structure that should be added
 */
void mod_bridge_vlanport_add(vlanport_info_t* vlanport) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(vlanport, exit, ERROR, "vlanport can not be NULL");
    if(vlanport_config_valid(vlanport)) {
        vlan_create_nm_query(vlanport);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Removes the vlan from the bridge and then destroys it
 * @param vlanport the vlanport info structure that should be removed
 */
void mod_bridge_vlanport_remove(vlanport_info_t* vlanport) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(vlanport, exit, ERROR, "vlanport can not be NULL");
    mod_bridging_execute_vlanport_function("remove-port", vlanport);
    mod_vlan_execute_function("destroy-vlan", vlanport);
    vlan_close_nm_query(vlanport);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Check if the bridge is configured validly, applies config accordingly
 * @param bridge_obj the bridge object pointer
 */
bridge_status_t bridge_check_and_apply_config(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    bool bridge_enabled = amxd_object_get_value(bool, bridge_obj, "Enable", NULL);
    bool was_enabled;
    bool valid_mp = false;
    bridge_status_t bridge_status = BRIDGE_ERROR;
    bridge_info_t* bridge = NULL;

    when_null_trace(bridge_obj, exit, ERROR, "bridge object is NULL");
    when_null_trace(bridge_obj->priv, exit, ERROR, "bridge private data is NULL");
    bridge = (bridge_info_t*) bridge_obj->priv;
    valid_mp = bridge_has_valid_mp(bridge_obj);

    if(bridge_enabled) {
        if(valid_mp) {
            bridge_status = BRIDGE_ENABLED;
        } else {
            bridge_status = BRIDGE_ERROR;
        }
    } else {
        bridge_status = BRIDGE_DISABLED;
    }

    was_enabled = bridge_is_enabled(bridge_obj);
    SAH_TRACEZ_INFO(ME, "Bridge '%s' status '%d'", bridge_obj->name, bridge_status);

    if(!bridge->added && valid_mp) {
        rv = mod_bridge_add(bridge_obj);
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to add bridge, rv = %d", rv);
        } else {
            bridge->added = true;
        }
    }

    switch(bridge_status) {
    case BRIDGE_ENABLED:
        if(!was_enabled) {
            mod_configure_stp(bridge_obj, NULL);
            rv = mod_bridge_enable(bridge_obj);
            break;
        }
        rv = 0;
        break;
    case BRIDGE_DISABLED:
        if(was_enabled) {
            rv = mod_bridge_disable(bridge_obj);
            break;
        }
        rv = 0;
        break;
    case BRIDGE_ERROR:
        if(was_enabled) {
            rv = mod_bridge_disable(bridge_obj);
            break;
        }
        rv = 0;
        break;
    }

    if(rv != 0) {
        bridge_status = BRIDGE_ERROR;
    }

    bridging_update_bridge_status(bridge_obj, bridge_status);

exit:
    SAH_TRACEZ_OUT(ME);
    return bridge_status;
}

/**
 * @brief Initialize a new bridge info structure
 * @param bridge pointer to bridge info structure
 */
amxd_status_t bridge_info_new(bridge_info_t** bridge) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(bridge, exit, ERROR, "bridge can not be null, invalid argument");

    *bridge = (bridge_info_t*) calloc(1, sizeof(bridge_info_t));
    when_null_status(*bridge, exit, rv = amxd_status_out_of_mem);

    (*bridge)->bridge_name = NULL;

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Cleanup a bridge info structure
 * @param bridge pointer to the bridge info structure
 */
amxd_status_t bridge_info_clean(bridge_info_t** bridge) {
    SAH_TRACEZ_IN(ME);
    if(!bridge || !(*bridge)) {
        goto exit;
    }
    free((*bridge)->bridge_name);
    free(*bridge);
    *bridge = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

/**
 * @brief Initialize a new port info structure
 * @param port pointer to port info structure
 * @param bridge the bridge object this port is part of
 * @param netdev_intf the name of this port as used by NetDev
 */
amxd_status_t port_info_new(port_info_t** port, amxd_object_t* bridge, const char* netdev_intf) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(port, exit, ERROR, "port can not be null, invalid argument");
    when_null_trace(bridge, exit, ERROR, "bridge can not be null, invalid argument");

    *port = (port_info_t*) calloc(1, sizeof(port_info_t));
    when_null_status(*port, exit, rv = amxd_status_out_of_mem);

    (*port)->bridge_obj = bridge;
    if(!str_empty(netdev_intf)) {
        (*port)->netdev_intf = strndup(netdev_intf, 64);
        when_null_status((*port)->netdev_intf, exit, rv = amxd_status_unknown_error);
    }

    (*port)->netdev_status_subscription = NULL;
    (*port)->netdev_new_link_subscription = NULL;
    (*port)->query = NULL;
    (*port)->last_change = get_system_uptime();

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Cleanup a port info structure
 * @param port pointer to the port info structure
 */
amxd_status_t port_info_clean(port_info_t** port) {
    SAH_TRACEZ_IN(ME);
    if(!port || !(*port)) {
        goto exit;
    }

    free((*port)->netdev_intf);
    amxb_subscription_delete(&(*port)->netdev_status_subscription);
    amxb_subscription_delete(&(*port)->netdev_new_link_subscription);
    netmodel_closeQuery((*port)->query);
    free(*port);
    *port = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

/**
 * @brief Initialize a new port info structure
 * @param vlanport pointer to port info structure
 * @param vlanport_obj the vlanport object
 * @param bridge_obj the bridge object this vlanport is part of
 * @param port_obj the port object that is linked to by the vlanport
 * @param vlan_obj the vlan object that is linked to by the vlanport
 * @param vlanport_name char pointer containing the name that will be given to the vlan interface
 *  if left empty, a name will be auto generated
 */
amxd_status_t vlanport_info_new(vlanport_info_t** vlanport,
                                amxd_object_t* vlanport_obj,
                                amxd_object_t* bridge_obj,
                                amxd_object_t* port_obj,
                                amxd_object_t* vlan_obj,
                                const char* vlanport_name) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(vlanport, exit, ERROR, "vlanport can not be null, invalid argument");
    when_null_trace(vlanport_obj, exit, ERROR, "bridge object can not be null, invalid argument");
    when_null_trace(bridge_obj, exit, ERROR, "bridge object can not be null, invalid argument");

    *vlanport = (vlanport_info_t*) calloc(1, sizeof(vlanport_info_t));
    when_null_status(*vlanport, exit, rv = amxd_status_out_of_mem);

    (*vlanport)->vlanport_obj = vlanport_obj;
    (*vlanport)->bridge_obj = bridge_obj;
    (*vlanport)->port_obj = port_obj;
    (*vlanport)->vlan_obj = vlan_obj;
    (*vlanport)->query = NULL;
    if((vlanport_name == NULL) || (*vlanport_name == 0)) {
        (*vlanport)->vlanport_name = NULL;
    } else {
        (*vlanport)->vlanport_name = strndup(vlanport_name, 16);
    }

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Cleanup a vlanport info structure
 * @param vlanport pointer to the vlanport info structure
 */
amxd_status_t vlanport_info_clean(vlanport_info_t** vlanport) {
    SAH_TRACEZ_IN(ME);
    if(!vlanport || !(*vlanport)) {
        goto exit;
    }

    netmodel_closeQuery((*vlanport)->query);
    free((*vlanport)->vlanport_name);
    free((*vlanport)->port_name);
    free(*vlanport);
    *vlanport = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

/**
 * @brief Call the bridging module to add a bridge and all of its ports
 * @param bridge_obj the bridge object pointer
 */
int mod_bridge_add(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null_trace(bridge_obj, exit, ERROR, "No bridge object provided");

    SAH_TRACEZ_INFO(ME, "Adding new bridge '%s'", bridge_obj->name);
    rv = mod_bridging_execute_bridge_function("add-bridge", bridge_obj);
    when_failed(rv, exit);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Call the bridging module to enable a bridge
 * @param bridge_obj the bridge object pointer
 */
int mod_bridge_enable(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* port_templ_obj = amxd_object_get_child(bridge_obj, "Port");
    amxd_object_t* vlanport_obj = amxd_object_get_child(bridge_obj, "VLANPort");

    amxd_object_for_all(port_templ_obj, "[Enable == true]", _port_add_cb, NULL);
    amxd_object_for_all(vlanport_obj, "[Enable == true]", _vlanport_add_cb, NULL);

    SAH_TRACEZ_INFO(ME, "Enabling bridge '%s'", bridge_obj->name);
    rv = mod_bridging_execute_bridge_function("enable-bridge", bridge_obj);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Call the bridging module to disable a bridge
 * @param bridge_obj the bridge object pointer
 */
int mod_bridge_disable(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* port_templ_obj = amxd_object_get_child(bridge_obj, "Port");
    amxd_object_t* vlanport_obj = amxd_object_get_child(bridge_obj, "VLANPort");
    amxd_object_for_all(port_templ_obj, "[Enable == true]", _port_remove_cb, NULL);
    amxd_object_for_all(vlanport_obj, "[Enable == true]", _vlanport_remove_cb, NULL);

    SAH_TRACEZ_INFO(ME, "Disabling bridge '%s'", bridge_obj->name);
    rv = mod_bridging_execute_bridge_function("disable-bridge", bridge_obj);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Call the bridging module to remove a bridge
 * @param bridge_obj the bridge object pointer
 */
void mod_bridge_remove(amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    bridge_info_t* bridge = NULL;

    when_null_trace(bridge_obj, exit, ERROR, "No bridge object provided");

    SAH_TRACEZ_INFO(ME, "Removing bridge '%s'", bridge_obj->name);
    mod_bridging_execute_bridge_function("remove-bridge", bridge_obj);
    when_null(bridge_obj, exit);
    when_null(bridge_obj->priv, exit);

    bridge = (bridge_info_t*) bridge_obj->priv;
    bridge_close_flag_query(bridge);
    bridge->added = false;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Translate the port type string to enum
 * @param port pointer to port info structure
 * @param type_str char* value containing the port type from the datamodel
 */
void port_type(port_info_t* port, const char* type_str) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(port, exit, ERROR, "port can not be NULL");
    when_str_empty(type_str, exit);

    if(strcmp(type_str, "ProviderNetworkPort") == 0) {
        port->port_type = PROVIDER_NETWORK_PORT;
    } else if(strcmp(type_str, "CustomerNetworkPort") == 0) {
        port->port_type = CUSTOMER_NETWORK_PORT;
    } else if(strcmp(type_str, "CustomerEdgePort") == 0) {
        port->port_type = CUSTOMER_EDGE_PORT;
    } else if(strcmp(type_str, "CustomerVLANPort") == 0) {
        port->port_type = CUSTOMER_VLAN_PORT;
    } else if(strcmp(type_str, "VLANUnawarePort") == 0) {
        port->port_type = VLAN_UNAWARE_PORT;
    } else {
        port->port_type = UNKNOWN_PORT_TYPE;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Apply the STP configuration for the given bridge
 * @param bridge_obj pointer bridge object for which to apply the STP configuration
 * @param args htable containing the STP parameters, is allowed to be NULL
 */
void mod_configure_stp(amxd_object_t* bridge_obj, amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    const char* status = NULL;
    amxd_trans_t trans;
    amxd_object_t* stp_obj = NULL;

    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    when_null_trace(bridge_obj, exit, ERROR, "Bridge object is NULL");

    if(mod_bridging_execute_stp_function("modify-bridge-stp", bridge_obj, args, &ret) != 0) {
        status = "Error";
    } else {
        status = GET_CHAR(&ret, "Status");
    }

    stp_obj = amxd_object_findf(bridge_obj, ".STP.");
    when_null_trace(stp_obj, exit, ERROR, "STP object is NULL");

    amxd_trans_select_object(&trans, stp_obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    when_failed_trace(amxd_trans_apply(&trans, bridge_get_dm()), exit, ERROR, "Failed to apply transaction");

exit:
    amxc_var_clean(&ret);
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return;
}

uint32_t get_system_uptime(void) {
    uint32_t uptime = 0;
    struct sysinfo info;
    when_failed_trace(sysinfo(&info), exit, ERROR, "Could not read total uptime of the system");
    uptime = info.uptime;
exit:
    return uptime;
}
