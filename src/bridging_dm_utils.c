/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "bridging_dm_utils.h"
#include "bridging_utils.h"

#define ME "bridging"

/**
 * @brief Changes the bridge/port status in the datamodel
 * @param obj the object pointer
 * @param new_status char* value containing the new status
 * @return amxd_status_ok when the all actions are applied, otherwise an other error code and no changes in the data model are done.
 */
static amxd_status_t dm_bridging_update_status(amxd_object_t* obj, const char* new_status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = bridge_get_dm();
    amxd_trans_t trans;

    when_null_trace(obj, exit, ERROR, "object should not be NULL, can not change state");
    when_str_empty(new_status, exit);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", new_status);
    rv = amxd_trans_apply(&trans, dm);

    amxd_trans_clean(&trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Changes the port name in the datamodel
 * @param obj the object pointer
 * @param new_name char* value containing the new name
 * @return amxd_status_ok when the all actions are applied, otherwise an other error code and no changes in the data model are done.
 */
static amxd_status_t dm_bridging_update_name(amxd_object_t* obj, const char* new_name) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = bridge_get_dm();
    amxd_trans_t trans;

    when_null_trace(obj, exit, ERROR, "object should not be NULL, can not change state");
    when_null(new_name, exit);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Name", new_name);
    rv = amxd_trans_apply(&trans, dm);

    amxd_trans_clean(&trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Updates bridge status
 * @param bridge_obj the bridge object pointer
 * @param bridge_status the next bridge status enum value
 */
amxd_status_t bridging_update_bridge_status(amxd_object_t* bridge_obj, bridge_status_t bridge_status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    const char* new_status = BRIDGE_STATUS_ERROR;

    switch(bridge_status) {
    case BRIDGE_DISABLED:
        new_status = BRIDGE_STATUS_DISABLED;
        break;
    case BRIDGE_ENABLED:
        new_status = BRIDGE_STATUS_ENABLED;
        break;
    case BRIDGE_ERROR:
        new_status = BRIDGE_STATUS_ERROR;
        break;
    default:
        new_status = BRIDGE_STATUS_DISABLED;
        break;
    }

    rv = dm_bridging_update_status(bridge_obj, new_status);
    if(rv == amxd_status_ok) {
        SAH_TRACEZ_INFO(ME, "status for bridge: %s set to %s", bridge_obj->name, new_status);
    } else {
        SAH_TRACEZ_ERROR(ME, "%s status could not be set to %s, because error %d",
                         bridge_obj->name, new_status, rv);
    }
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Updates port status
 * @param port the port info structure that should be added
 * @param port_status char* value containing the next port status
 */
amxd_status_t bridging_update_port_status(port_info_t* port, const char* port_status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    const char* new_status = NULL;

    when_null_trace(port, exit, ERROR, "port should not be NULL, can not change state");
    when_null_trace(port->port_obj, exit, ERROR, "port object should not be NULL, can not change state");
    when_str_empty(port_status, exit);

    if(strcmp(port_status, "notpresent") == 0) {
        new_status = "NotPresent";
    } else if(strcmp(port_status, "down") == 0) {
        new_status = "Down";
    } else if(strcmp(port_status, "lowerlayerdown") == 0) {
        new_status = "LowerLayerDown";
    } else if(strcmp(port_status, "dormant") == 0) {
        new_status = "Dormant";
    } else if(strcmp(port_status, "up") == 0) {
        new_status = "Up";
    } else if(strcmp(port_status, "Error") == 0) {
        new_status = "Error";
    } else {
        new_status = "Unknown";
    }

    rv = dm_bridging_update_status(port->port_obj, new_status);
    when_failed(rv, exit);

    port->last_change = get_system_uptime();

    SAH_TRACEZ_INFO(ME, "status for port: %s set to %s", port->port_obj->name, new_status);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Updates port name
 * @param port pointer to the port instance object
 * @param name char pointer value containing the next port name
 */
amxd_status_t bridging_update_port_name(amxd_object_t* port_obj, const char* name) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(port_obj, exit, ERROR, "port object should not be NULL, can not change name");
    when_null(name, exit);

    rv = dm_bridging_update_name(port_obj, name);
    when_failed_trace(rv, exit, ERROR, "Failed to set Name for port '%s' to '%s'", port_obj->name, name);

    SAH_TRACEZ_INFO(ME, "Name for port: %s set to %s", port_obj->name, name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
