/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_dir.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "mstpd.h"

#define ME "mod-stp"

#define BUF_SIZE 32
#define PATH_SIZE 100
#define STP_STATE_PATH "/sys/class/net/%s/bridge/stp_state"

static amxc_var_t* history;
static bool awaiting_ctx = false;

static int call_mstpd(amxc_var_t* args);

static void init_history(void) {
    if(history == NULL) {
        amxc_var_new(&history);
        amxc_var_set_type(history, AMXC_VAR_ID_LIST);
    }
}

static void clear_history(void) {
    if(history != NULL) {
        amxc_var_delete(&history);
        history = NULL;
    }
}

static void append_history(amxc_var_t* el) {
    if(history != NULL) {
        amxc_var_t* new_var = amxc_var_add_new(history);
        amxc_var_copy(new_var, el);
    }
}

static int file_read(const cstring_t path, char* buf, uint32_t size) {
    FILE* fp = NULL;
    int rv = -1;
    char* s = NULL;

    when_null(buf, exit);

    fp = fopen(path, "r");
    when_null_trace(fp, exit, ERROR, "Failed to open %s (%s)", path, strerror(errno));
    s = fgets(buf, size, fp);
    when_null_trace(s, exit, ERROR, "Failed to read %s", path);

    rv = atoi(buf);
exit:
    if(fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    return rv;
}

static void file_write(const cstring_t intf_name, const cstring_t file_path, long long unsigned int value) {
    char path[PATH_SIZE] = {0};
    char buf[BUF_SIZE] = {0};
    FILE* fp = NULL;
    uint64_t read_value = 0;
    int ret_val = -1;

    when_str_empty_trace(intf_name, exit, ERROR, "Bad intf_name param given");
    when_str_empty_trace(file_path, exit, ERROR, "Bad parameter id given, has an empty file_path");

    snprintf(path, sizeof(path), file_path, intf_name);

    read_value = file_read(path, buf, sizeof(buf));

    if(read_value != value) {
        fp = fopen(path, "w");
        when_null_trace(fp, exit, ERROR, "Failed to open file at path %s (%s)", path, strerror(errno));

        ret_val = fprintf(fp, "%llu", value);
        when_false_trace(ret_val >= 0, exit, ERROR, "Failed to write to the file at path %s", path);
        SAH_TRACEZ_INFO(ME, "Succesfully written the value %llu to %s", value, path);
    } else {
        SAH_TRACEZ_INFO(ME, "No need to write %llu to %s", value, path);
    }
exit:
    if(fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    return;
}

static int stp_read_status(const char* bridge) {
    char path[PATH_SIZE] = {0};
    char buf[BUF_SIZE] = {0};

    snprintf(path, sizeof(path), "/sys/class/net/%s/bridge/stp_state", bridge);

    return file_read(path, buf, sizeof(buf));
}

static void wait_done_cb(UNUSED const char* const signal,
                         UNUSED const amxc_var_t* const date,
                         UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "wait:MSTPD. received");
    amxp_slot_disconnect(NULL, "wait:MSTPD.", wait_done_cb);

    awaiting_ctx = false;
    amxc_var_for_each(var, history) {
        call_mstpd(var);
        amxc_var_delete(&var);
    }
}

static void normalize_params(amxc_var_t* params) {
    amxc_var_t* var = amxc_var_get_key(params, "HelloTime", AMXC_VAR_FLAG_DEFAULT);
    if(var != NULL) {
        amxc_var_set(uint32_t, var, GET_UINT32(var, NULL) / 100); // convert from centiseconds to seconds
    }

    var = amxc_var_get_key(params, "MaxAge", AMXC_VAR_FLAG_DEFAULT);
    if(var != NULL) {
        amxc_var_set(uint32_t, var, GET_UINT32(var, NULL) / 100); // convert from centiseconds to seconds
    }
}

static void start_mstpd(void) {
    amxp_subproc_t* proc = NULL;
    int status = -1;

    amxp_subproc_new(&proc);
    amxp_subproc_start_wait(proc, 1500, (char*) "sh", "/etc/init.d/mstpd", "start", NULL);
    status = amxp_subproc_get_exitstatus(proc);

    if(status != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to start mstpd daemon");
    }

    amxp_subproc_delete(&proc);
}

int call_mstpd(amxc_var_t* args) {
    amxb_bus_ctx_t* ctx = amxb_be_who_has("MSTPD.");
    amxc_var_t params;
    amxc_var_t ret;
    amxc_var_t* arguments = NULL;
    int rv = -1;

    amxc_var_init(&params);
    amxc_var_init(&ret);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    if(ctx == NULL) {
        SAH_TRACEZ_WARNING(ME, "MSTPD. object is not yet available");
        if(!awaiting_ctx) {
            awaiting_ctx = true;
            amxp_slot_connect_filtered(NULL, "wait:MSTPD.", NULL, wait_done_cb, NULL);
            amxb_wait_for_object("MSTPD."); // works async
        }
        append_history(args);
        goto exit;
    }

    arguments = amxc_var_add_new_key(&params, "arguments");
    amxc_var_move(arguments, args);
    normalize_params(arguments);

    rv = amxb_call(ctx, "MSTPD.", "STPControl", &params, &ret, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to call MSTPD.STPControl(), error code %d", rv);

exit:
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
    return rv;
}

static int is_stp_enabled(UNUSED const char* function_name,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const cstring_t bridge_name = GET_CHAR(args, "bridge_name");
    when_str_empty_trace(bridge_name, exit, ERROR, "Bridge name is empty, can't read STP status");

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, ret, "Enabled", (stp_read_status(bridge_name) != 0));

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int modify_bridge_stp(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* status = NULL;
    const cstring_t bridge_name = GET_CHAR(args, "bridge_name");
    amxc_var_t* enable = GET_ARG(args, "Enable");
    when_str_empty_trace(bridge_name, exit, ERROR, "Bridge name is empty, can't configure STP");

    if(enable != NULL) {
        if(GET_BOOL(enable, NULL)) {
            file_write(bridge_name, STP_STATE_PATH, 1);
        } else {
            file_write(bridge_name, STP_STATE_PATH, 0);
        }
    }

    rv = call_mstpd(args);
    if(rv != 0) {
        SAH_TRACEZ_WARNING(ME, "Something went wrong while configuring STP for bridge %s (%u)", bridge_name, rv);
        file_write(bridge_name, STP_STATE_PATH, 0);
        status = "Error";
    } else {
        status = enable ? "Enabled" : "Disabled";
    }

    rv = 0;
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "Status", status);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static AMXM_CONSTRUCTOR stp_mstpd_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    start_mstpd();
    init_history();
    when_failed_trace(amxm_module_register(&mod, so, MOD_STP_CTRL),
                      exit, ERROR, "Failed to register module %s", MOD_STP_CTRL);
    when_failed_trace(amxm_module_add_function(mod, "modify-bridge-stp", modify_bridge_stp),
                      exit, ERROR, "Failed to register function modify-bridge-stp");
    when_failed_trace(amxm_module_add_function(mod, "is-stp-enabled", is_stp_enabled),
                      exit, ERROR, "Failed to register function is-stp-enabled");
exit:
    return 0;
}

static AMXM_DESTRUCTOR stp_mstpd_stop(void) {
    clear_history();
    return 0;
}
