/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>

#include "test_module.h"
#include "mstpd.h"
#include "mock_dm.h"

#define MOD_NAME "target_module"
#define MOD_PATH "../modules_under_test/" MOD_NAME ".so"

#define handle_events amxut_bus_handle_events

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;


int test_setup(UNUSED void** state) {
    amxut_bus_setup(state);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();

    mock_dm_init(parser, amxd_dm_get_root(dm));
    amxp_sigmngr_add_signal(NULL, "wait:MSTPD.");

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxut_bus_teardown(state);

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, MOD_NAME, MOD_PATH), 0);
}

void test_has_functions(UNUSED void** state) {
    assert_true(amxm_has_function(MOD_NAME, MOD_STP_CTRL, "modify-bridge-stp"));
    assert_true(amxm_has_function(MOD_NAME, MOD_STP_CTRL, "is-stp-enabled"));
}

void test_modify_stp(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "bridge_name", "br-lan");
    amxc_var_add_key(bool, &var, "Enable", true);
    amxc_var_add_key(uint32_t, &var, "AgingTime", 300);
    amxc_var_add_key(uint32_t, &var, "BridgePriority", 32768);
    amxc_var_add_key(uint32_t, &var, "HelloTime", 200);
    amxc_var_add_key(uint32_t, &var, "MaxAge", 2000);
    amxc_var_add_key(uint32_t, &var, "ForwardingDelay", 15);

    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_STP_CTRL, "modify-bridge-stp", &var, &ret), 0);
    assert_string_equal(GET_CHAR(&ret, "Status"), "Error");

    amxc_var_clean(&var);
    amxc_var_clean(&ret);

    expect_function_call(_STPControl);
    amxp_sigmngr_emit_signal(NULL, "wait:MSTPD.", NULL);
    handle_events();
}

void test_can_close_module(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
}
